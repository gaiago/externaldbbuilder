package com.gaiago.movensdb

import org.junit.jupiter.api.BeforeEach
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
open class ContainerTest {

    @BeforeEach
    fun setUp() {
        MovensDb.cleanup()
    }

    companion object {
        @Container
        val postgres = ContainerPostgres()
    }
}