package com.gaiago.movensdb

class CoreRole : Entity() {
    companion object {
        val ADMINISTRATOR = CoreRole().apply { id = 1 }
        val POWER_USER = CoreRole().apply { id = 2 }
        val END_USER = CoreRole().apply { id = 3 }
        val VEHICLES_ADMINISTRATOR = CoreRole().apply { id = 4 }
        val VEHICLES_POWER_USER = CoreRole().apply { id = 5 }
        val CONTROL_ROOM_USER = CoreRole().apply { id = 6 }
    }
}