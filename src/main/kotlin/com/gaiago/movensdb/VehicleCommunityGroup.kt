package com.gaiago.movensdb

class VehicleCommunityGroup(private val community: VehicleCommunity, private val group: VehicleGroup) {
    fun save(): VehicleCommunityGroup {
        MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "CommunityGroups" ("GroupID", "CommunityID") VALUES (?, ?)"""
            ).also {
                it.setInt(1, group.id)
                it.setInt(2, community.id)
            }.executeUpdate()
        }

        return this
    }
}