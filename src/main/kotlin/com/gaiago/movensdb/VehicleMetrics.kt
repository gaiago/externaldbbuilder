package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS
import java.util.*

class VehicleMetrics(
    val vehicle: VehicleVehicle = VehicleVehicle().save(),
    val name: UUID = UUID.randomUUID(),
    val value: Double = 0.0
) : Entity() {
    fun save() : VehicleMetrics {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "VehicleMetrics" ("VehicleID", "Name", "LastValue", "Start") VALUES
                    (?, ?, ?, '2010-01-01')""", RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, vehicle.id)
                it.setObject(2, name)
                it.setDouble(3, value)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}
