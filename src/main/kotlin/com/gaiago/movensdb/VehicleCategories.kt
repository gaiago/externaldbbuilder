package com.gaiago.movensdb

import java.sql.Statement
import java.util.*
import java.util.UUID.fromString

class VehicleCategories(
    val guid: UUID = UUID.randomUUID(),
    val labels: String = """{"en":"City Car","it":"City Car"}"""
) : Entity() {
    fun save(): VehicleCategories {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "VehicleCategories" ("CreatedDate", "Labels", "Guid") VALUES ('2019-01-01', ?, ?)""",
                Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setString(1, labels)
                it.setObject(2, guid)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }

    companion object {
        fun load(id: Int): VehicleCategories {
            return MovensDb.vehicle().use { connection ->
                connection.prepareStatement(
                    """SELECT * FROM "VehicleCategories" WHERE "ID" = ?"""
                ).also {
                    it.setInt(1, id)
                }.executeQuery().let {
                    it.next()
                    VehicleCategories(fromString(it.getString("Guid")), it.getString("Labels")).apply { this.id = id }
                }
            }
        }
    }
}