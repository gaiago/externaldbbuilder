package com.gaiago.movensdb

import com.gaiago.movensdb.CoreTripEventSource.USER_DEVICE
import java.sql.Statement.RETURN_GENERATED_KEYS
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

enum class CoreTripEventSource {
    ONBOARD_DEVICE,
    USER_DEVICE;

    fun value(): Int {
        return when (this) {
            ONBOARD_DEVICE -> 0
            USER_DEVICE -> 1
        }
    }
}

class CoreTripEvents(val trip: CoreTrips, val type: CoreTripEventType, val timestamp: LocalDateTime, val source: CoreTripEventSource = USER_DEVICE) : Entity() {
    fun save(): CoreTripEvents {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                """
                INSERT INTO "TripEvents" 
                ("TripID", "Guid", "CreatedDate", "Timestamp", "Source", "Type") 
                VALUES (?, ?, '2019-01-01', ?, ?, ?)
                """, RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, trip.id)
                it.setObject(2, UUID.randomUUID())
                it.setTimestamp(3, Timestamp.valueOf(timestamp))
                it.setInt(4, source.value())
                it.setInt(5, type.value)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }
        return this
    }

}
