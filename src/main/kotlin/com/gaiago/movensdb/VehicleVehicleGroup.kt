package com.gaiago.movensdb

class VehicleVehicleGroup(private val vehicle: VehicleVehicle, private val group: VehicleGroup) {
    fun save() {
        MovensDb.vehicle().use { connection ->
            connection.prepareStatement("""INSERT INTO "VehicleGroup" ("VehicleID", "GroupID") VALUES (?, ?)""")
                .also {
                    it.setInt(1, vehicle.id)
                    it.setInt(2, group.id)
                }.executeUpdate()
        }
    }
}
