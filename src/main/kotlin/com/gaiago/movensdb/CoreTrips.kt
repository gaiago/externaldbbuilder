package com.gaiago.movensdb

import com.gaiago.movensdb.CoreTripEventType.BEGIN
import com.gaiago.movensdb.CoreTripEventType.END
import java.sql.Statement.RETURN_GENERATED_KEYS
import java.sql.Timestamp
import java.sql.Types.TIMESTAMP
import java.time.LocalDateTime
import java.util.*

class CoreTrips(
    val guid: UUID = UUID.randomUUID(),
    val movementAuthority: CoreMovementAuthority = CoreMovementAuthority().save(),
    val startTime: LocalDateTime = LocalDateTime.parse("1900-01-01T00:00:00"),
    val endTime: LocalDateTime? = null
) : Entity() {

    val user = movementAuthority.transportationRequest.user
    val equipmentReference = movementAuthority.equipmentReference

    fun save(): CoreTrips {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                """
                    INSERT INTO "Trips" 
                    ("MovementAuthorityID", "UserID", "CreatedDate", "Guid", "IsCompleted", "IsBillable", "StartTimestamp", "EndTimestamp", "Status", "Index", "EquipmentReference") 
                    VALUES (?, ?, '2019-01-01', ?, ?, true, ?, ?, 4, 0, ?)
                    """, RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, movementAuthority.id)
                it.setInt(2, movementAuthority.transportationRequest.user.id)
                it.setObject(3, guid)
                it.setBoolean(4, endTime != null)
                it.setTimestamp(5, Timestamp.valueOf(startTime))
                if (endTime != null) {
                    it.setTimestamp(6, Timestamp.valueOf(endTime))
                } else {
                    it.setNull(6, TIMESTAMP)
                }
                it.setString(7, movementAuthority.equipmentReference)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        CoreTripEvents(this, BEGIN, startTime).save()

        if (endTime != null) {
            CoreTripEvents(this, END, endTime).save()
        }

        return this
    }
}
