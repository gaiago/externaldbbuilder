package com.gaiago.movensdb

class CoreUserRole(val user: CoreUser, val role: CoreRole) {
    fun save(): CoreUserRole {
        MovensDb.core().use { connection ->
            connection.prepareStatement(
                """
                    INSERT INTO "UserRole" 
                    ("UserID", "RoleID") 
                    VALUES (?, ?)
                    """
            ).also {
                it.setInt(1, user.id)
                it.setInt(2, role.id)
            }.executeUpdate()
        }

        return this
    }
}