package com.gaiago.movensdb

import java.time.LocalDateTime
import java.util.*

class CommonCommunity(
        val guid: UUID = UUID.randomUUID(),
        name: String = "Hotel",
        userGroups: List<CoreGroup> = listOf(CoreGroup(communities = emptyList(), users = emptyList()).save()),
        users: List<CoreUser> = listOf(CoreUser(communities = emptyList(), groups = userGroups).save()),
        vehicleGroups: List<VehicleGroup> = listOf(VehicleGroup(communities = emptyList()).save()),
        zones: List<VehicleZone> = listOf(VehicleZone(rules = emptyList()).save()),
        registrationCode: String = "REGISTRATIONCODE",
        deletionDate: LocalDateTime? = null
) : Entity() {
    val coreDb = CoreCommunity(guid, name, userGroups, users, registrationCode, deletionDate)
    val vehicleDb = VehicleCommunity(guid, name, vehicleGroups, zones, deletionDate)

    fun save(): CommonCommunity {
        coreDb.save()
        vehicleDb.save()
        return this
    }
}