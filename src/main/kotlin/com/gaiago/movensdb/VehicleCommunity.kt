package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

class VehicleCommunity(
    val guid: UUID = UUID.randomUUID(),
    val name: String = "Hotel",
    var vehicleGroups: List<VehicleGroup> = listOf(VehicleGroup(communities = emptyList()).save()),
    var vehicleZones: List<VehicleZone> = listOf(VehicleZone(rules = emptyList()).save()),
    val deletionDate: LocalDateTime? = null
) : Entity() {
    fun save(): VehicleCommunity {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "Communities" ("Name", "CreatedDate", "LastUpdated", "Level", "Guid", "DeletionDate") VALUES (?, '2019-01-01', '2019-01-01', 0, ?, ?) """,
                RETURN_GENERATED_KEYS
            ).also {
                it.setString(1, name)
                it.setObject(2, guid)
                it.setTimestamp(3, if (deletionDate == null) null else Timestamp.valueOf(deletionDate))
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        vehicleGroups.forEach {
            VehicleCommunityGroup(this, it).save()
            it.add(this)
        }

        vehicleZones.forEach {
            VehicleCommunityZone(this, it).save()
        }

        return this
    }

    fun add(group: VehicleGroup) {
        vehicleGroups = vehicleGroups.plus(group)
    }
}