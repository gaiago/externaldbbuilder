package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

class CoreCommunity(
    val guid: UUID = UUID.randomUUID(),
    val name: String = "Hotel",
    var groups: List<CoreGroup> = listOf(CoreGroup(communities = emptyList(), users = emptyList()).save()),
    var users: List<CoreUser> = listOf(CoreUser(communities = emptyList(), groups = groups).save()),
    val registrationCode: String = "REGISTRATIONCODE",
    val deletionDate: LocalDateTime? = null
) : Entity() {
    fun save(): CoreCommunity {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "Communities" ("Name", "CreatedDate", "LastUpdated", "Level", "Guid", "RegistrationCode", "DeletionDate") VALUES (?, '2019-01-01', '2019-01-01', 0, ?, ?, ?) """,
                RETURN_GENERATED_KEYS
            ).also {
                it.setString(1, name)
                it.setObject(2, guid)
                it.setString(3, registrationCode)
                it.setTimestamp(4, if (deletionDate == null) null else Timestamp.valueOf(deletionDate))
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        groups.forEach {
            CoreCommunityGroup(this, it).save()
            it.add(this)
        }

        users.forEach {
            CoreJoinedCommunity(it, this).save()
            it.add(this)
        }

        return this
    }

    fun add(coreGroup: CoreGroup) {
        groups = groups.plus(coreGroup)
    }

    fun add(coreUser: CoreUser) {
        users = users.plus(coreUser)
    }
}