package com.gaiago.movensdb

import java.sql.Statement

class CoreIssuingAuthorities() : Entity() {
    fun save(): CoreIssuingAuthorities {
        MovensDb.core().use { connection ->
            id = connection.prepareStatement(
                """
                    INSERT INTO "IssuingAuthorities" 
                    ("CreatedDate", "LastUpdated", "Name", "Code", "IsToBeVerified", "CountryCode") 
                    VALUES ('2019-01-01', '2019-01-01', 'Motorizzazione Civile', 'MCTC', true, 'ITA')
                    """, Statement.RETURN_GENERATED_KEYS
            ).also {
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}