package com.gaiago.movensdb

class CoreServiceRule(private val coreGroup: CoreGroup, private val vehicleGroup: VehicleGroup) {
    fun save(): CoreServiceRule {
        MovensDb.core().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "ServiceRules" ("GroupID", "ExtResourceGroupID", "CreatedDate", "LastUpdated") VALUES
               (?, ?, '2019-01-01', '2019-01-01')"""
            ).also {
                it.setInt(1, coreGroup.id)
                it.setInt(2, vehicleGroup.externalId)
            }.executeUpdate()
        }

        return this
    }
}