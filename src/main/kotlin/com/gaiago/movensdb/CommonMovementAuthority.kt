package com.gaiago.movensdb

import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.util.*

class CommonMovementAuthority(
    val guid: UUID = UUID.randomUUID(),
    createdDate: LocalDateTime = now(),
    deletionTime: LocalDateTime? = null,
    confirmed: Boolean = true,
    transportationRequest: CoreTransportationRequest = CoreTransportationRequest().save(),
    vehicle: VehicleVehicle? = null
) {

    val coreDb = CoreMovementAuthority(
            guid,
            createdDate,
            deletionTime,
            confirmed,
            if(vehicle == null) null else "${vehicle.plate};${vehicle.sticker ?: ""};${vehicle.vin ?: ""}",
            transportationRequest
    )

    val vehicleDb = VehicleMovementAuthority(
            guid,
            transportationRequest.guid,
            createdDate,
            transportationRequest.startTime,
            transportationRequest.endTime,
            deletionTime,
            confirmed,
            vehicle
    )

    fun save(): CommonMovementAuthority {
        coreDb.save()
        vehicleDb.save()
        return this
    }
}
