package com.gaiago.movensdb

import java.sql.Statement
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

class CoreTransportationRequest(
    val guid: UUID = UUID.randomUUID(),
    val startTime: LocalDateTime = LocalDateTime.now(),
    val endTime: LocalDateTime = LocalDateTime.now().plusHours(1),
    val user: CoreUser = CoreUser().save()
) : Entity() {

    fun save(): CoreTransportationRequest {
        MovensDb.core().use { connection ->
            id = connection.prepareStatement(
                """
                    INSERT INTO "TransportationRequests" 
                    ("UserID", "CreatedDate", "LastUpdated", "StartTime", "EndTime", "Guid") 
                    VALUES (?, '2019-01-01', '2019-01-01', ?, ?, ?)
                    """, Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, user.id)
                it.setTimestamp(2, Timestamp.valueOf(startTime))
                it.setTimestamp(3, Timestamp.valueOf(endTime))
                it.setObject(4, guid)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}
