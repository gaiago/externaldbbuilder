package com.gaiago.movensdb

enum class CoreAdditionalUserDataType(val code: Int) {
    NUMBER(0),
    BOOLEAN(1),
    STRING(2)
}