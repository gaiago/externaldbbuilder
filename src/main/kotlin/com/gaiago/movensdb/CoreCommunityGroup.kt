package com.gaiago.movensdb

class CoreCommunityGroup(private val coreCommunity: CoreCommunity, private val coreGroup: CoreGroup) {
    fun save(): CoreCommunityGroup {
        MovensDb.core().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "CommunityGroup" ("GroupID", "CommunityID") VALUES (?, ?)"""
            ).also {
                it.setInt(1, coreGroup.id)
                it.setInt(2, coreCommunity.id)
            }.executeUpdate()
        }

        return this
    }
}