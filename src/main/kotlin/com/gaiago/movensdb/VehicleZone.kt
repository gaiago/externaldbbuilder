package com.gaiago.movensdb

import java.sql.Statement
import java.util.*

class VehicleZone(
    val guid: UUID = UUID.randomUUID(),
    var rules: List<VehicleRules> = listOf(VehicleRules(zones = emptyList()).save())
) : Entity() {

    fun save(): VehicleZone {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "Zones" ("CreatedDate", "LastUpdated", "IsPaidParking", "CanRefuelElectricVehicles", "CanRefuelICEVehicles", "IsFreefloatingArea", "IsRoundtripArea", "IsAvailable", "ParkingSpots", "Guid") VALUES ('2019-01-01', '2019-01-01', true, true, true, true, true, true, 0, ?) """,
                Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setObject(1, guid)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        rules.forEach {
            VehicleZoneRules(it, this).save()
            it.add(this)
        }

        return this
    }

    fun add(vehicleRule: VehicleRules) {
        rules = rules.plus(vehicleRule)
    }
}