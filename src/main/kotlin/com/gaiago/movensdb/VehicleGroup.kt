package com.gaiago.movensdb

import java.sql.Statement
import java.util.*

class VehicleGroup(
    val guid: UUID = UUID.randomUUID(),
    var vehicles: List<VehicleVehicle> = listOf(VehicleVehicle(groups = emptyList()).save()),
    var communities: List<VehicleCommunity> = listOf(VehicleCommunity(vehicleGroups = emptyList()).save())
) : Entity() {
    var externalId = 0

    fun save(): VehicleGroup {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "Groups"
                    ("CreatedDate", "LastUpdated", "Guid", "DeletionDate") VALUES
                    ('2019-01-01', '2019-01-01', ?, NULL)""",
                Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setObject(1, guid)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        vehicles.forEach {
            VehicleVehicleGroup(it, this).save()
            it.add(this)
        }

        communities.forEach {
            VehicleCommunityGroup(it, this).save()
            it.add(this)
        }

        externalId = saveExternalResourceGroup()

        return this
    }

    fun add(community: VehicleCommunity) {
        communities = communities.plus(community)
    }

    fun add(vehicle: VehicleVehicle) {
        vehicles = vehicles.plus(vehicle)
    }

    private fun saveExternalResourceGroup(): Int {
        MovensDb.core().use { connection ->
            val moduleId = connection.prepareStatement(
                """INSERT INTO "Modules"
                            ("LastAnnounced") VALUES
                            ('2019-01-01')""", Statement.RETURN_GENERATED_KEYS
            ).also { it.executeUpdate() }.generatedKeys.also { it.next() }.getInt(1)

            return connection.prepareStatement(
                """INSERT INTO "ExtResourceGroups"
                            ("CreatedDate", "LastUpdated", "OriginatorModule", "Guid", "ModuleID") VALUES
                            ('2019-01-01', '2019-01-01', ?, ?, ?)""",
                Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setObject(1, UUID.randomUUID())
                it.setObject(2, guid)
                it.setInt(3, moduleId)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }
    }
}