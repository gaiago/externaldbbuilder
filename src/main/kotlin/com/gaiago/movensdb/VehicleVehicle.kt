package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

class VehicleVehicle(
    val guid: UUID = UUID.randomUUID(),
    val adminOperativeStatus: Boolean = true,
    val systemOperativeStatus: Boolean = true,
    val deletionDate: LocalDateTime? = null,
    val plate: String = Random().nextInt().toString(),
    val sticker: String? = null,
    val vin: String? = null,
    val notes: String? = null,
    val nominalGarage: VehicleGarage = VehicleGarage().save(),
    val category: VehicleCategories = VehicleCategories().save(),
    var groups: List<VehicleGroup> = listOf(VehicleGroup(vehicles = emptyList()).save()),
    val overriddenSystemOperativeStatus: Boolean = false,
    val odometerKm: Double? = null,
    val fuelLevelPct: Double? = null
) : Entity() {
    var device: VehicleOnBoardDevice? = null
        private set

    fun save(): VehicleVehicle {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "Vehicle"
                    ("CategoryID", "Guid", "NominalGarageID", "CreatedDate", "LastUpdated", "LastSetOperativeById", "SystemOperativeStatus", "AdminOperativeStatus", "DeletionDate", "LicensePlate", "Sticker", "VIN", "Notes", "OverriddenSystemOperativeStatus", "OdometerKm", "FuelLevelPct") VALUES
                    (?, ?, ?, '2019-01-01', '2019-01-01', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, category.id)
                it.setObject(2, guid)
                it.setInt(3, nominalGarage.id)
                it.setObject(4, UUID.randomUUID())
                it.setBoolean(5, systemOperativeStatus)
                it.setBoolean(6, adminOperativeStatus)
                it.setTimestamp(7, if (deletionDate == null) null else Timestamp.valueOf(deletionDate))
                it.setString(8, plate)
                it.setString(9, sticker)
                it.setString(10, vin)
                it.setString(11, notes)
                it.setBoolean(12, overriddenSystemOperativeStatus)
                it.setObject(13, odometerKm)
                it.setObject(14, fuelLevelPct)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        groups.forEach {
            VehicleVehicleGroup(this, it).save()
            it.add(this)
        }

        return this
    }

    fun add(group: VehicleGroup) {
        groups = groups.plus(group)
    }

    fun update(value: VehicleOnBoardDevice) {
        device = value
    }
}
