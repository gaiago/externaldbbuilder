package com.gaiago.movensdb

enum class CoreTripEventType(val value: Int) {
    PRE_BEGIN(0),
    BEGIN(1),
    PRE_END(2),
    END(3),
    ABANDON(4),
    PARK_ENTER(5),
    PARK_EXIT(6),
    SWITCH_USAGE_MODE(7),
    VEHICLE_OPERATION(8),
    USER_FEEDBACK(9),
    VEHICLE_FEEDBACK(10);

    companion object {
        fun from(value: Int): CoreTripEventType {
            return values().single { it.value == value }
        }
    }
}