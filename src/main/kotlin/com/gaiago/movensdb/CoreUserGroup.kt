package com.gaiago.movensdb

class CoreUserGroup(private val user: CoreUser, private val group: CoreGroup) {
    fun save(): CoreUserGroup {
        MovensDb.core().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "UserGroup" ("GroupID", "UserID") VALUES (?, ?)"""
            ).also {
                it.setInt(1, group.id)
                it.setInt(2, user.id)
            }.executeUpdate()
        }

        return this
    }
}