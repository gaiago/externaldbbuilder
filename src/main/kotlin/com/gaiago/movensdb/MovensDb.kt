package com.gaiago.movensdb

import java.sql.Connection
import java.sql.DriverManager.getConnection

object MovensDb {
    private val dbHost = getenvOrDefault("DATABASE_HOST", "localhost")
    private val dbUser = getenvOrDefault("DATABASE_USER", "postgres")
    private val dbPassword = getenvOrDefault("DATABASE_PASSWORD", "pg_password")

    fun core(): Connection {
        return getConnection(connectionUrl("movens"), dbUser, dbPassword)
    }

    fun vehicle(): Connection {
        return getConnection(connectionUrl("movens_vehicle"), dbUser, dbPassword)
    }

    fun cleanup() {
        core().use { connection ->
            connection.prepareStatement("""TRUNCATE "AdditionalUserDataFields", "Attachments", "CommunityLevels", "Facts", "Groups", "IssuingAuthorities", "MessageTemplates", "Modules", "Users", "__EFMigrationsHistory", "AdditionalUserData", "AuthenticatedSessions", "Communities", "CommunityDetail", "CommunityGroup", "ExtResourceFilters", "ExtResourceGroups", "JoinedCommunity", "LicenseDocuments", "NotificationPreferences", "Place", "ServiceRules", "TransportationRequests", "UserGroup", "UserRole", "BulletinBoardDocuments", "LicenseDocumentValidations", "MovementAuthorities", "MovementDetails", "TRFilters", "Trips", "TripDetails", "TripEvents" CASCADE""").execute()
            connection.prepareStatement("""INSERT INTO "Roles" ("ID", "LastUpdated", "ParentID", "Name", "Slug", "IsBackend", "IsFrontend", "IsOperator") VALUES
                (1,	'2020-01-23 08:31:27.568835',	null,	'Administrator',	'Admin',	'1',	'0',	'1'),
                (2,	'2020-01-23 08:31:27.568837',	null,	'Power User',	'PowerUser',	'1',	'0',	'1'),
                (3,	'2020-01-23 08:31:27.568838',	null,	'End User',	'EndUser',	'0',	'1',	'0'),
                (4,	'2020-07-30 13:08:03.337208',	null,	'Vehicles Administrator',	'V-Admin',	'1',	'0',	'1'),
                (5,	'2020-07-30 13:08:03.337209',	null,	'Vehicles Power User',	'V-PowerUser',	'1',	'0',	'1'),
                (6,	'2020-07-30 13:08:03.337206',	null,	'Control Room User',	'ControlRoom',	'1',	'0',	'1');""").execute()
        }

        vehicle().use { connection ->
            connection.prepareStatement("""TRUNCATE "Attachments", "DriverSettings", "Facts", "Filters", "Garage", "Groups", "MaintenanceOperationTypes", "Modules", "PointsOfInterest", "VehicleOptionals", "Zones", "__EFMigrationsHistory", "Communities", "CommunityGroups", "Roles", "Rules", "VehicleCategories", "ZoneRules", "Vehicle", "VehicleGroup", "VehicleLogs", "VehicleMetrics", "VehicleOptionalMatch", "VehicleSchedules", "VehicleTrips", "MaintenanceOperations", "MovementAuthorities", "MovementDetails", "OnBoardDevices", "Supplies", "VehicleTripLogs", "BillableTrips", "DamageReports", "DeviceActions", "DeviceEvents", "DeviceFeedbacks", "DeviceSecrets", "MaintenanceOperationRows", "BillableTripDetails", "BillableTripEvents" CASCADE""").execute()
        }
    }

    private fun connectionUrl(database: String) = "${baseConnectionUrl()}$database"

    private fun baseConnectionUrl() = "jdbc:postgresql://$dbHost/"

    private fun getenvOrDefault(value: String, default: String) = System.getenv(value) ?: default
}
