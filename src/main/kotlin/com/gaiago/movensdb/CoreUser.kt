package com.gaiago.movensdb

import java.sql.Statement
import java.time.LocalDateTime
import java.util.*

class CoreUser(
    val guid: UUID = UUID.randomUUID(),
    var communities: List<CoreCommunity> = listOf(CoreCommunity(users = emptyList(), groups = emptyList()).save()),
    val username: String = "${Random().nextInt()}@b.c",
    val email: String? = username,
    val fullName: String? = username,
    val phoneNumber: String? = null,
    val deletionDate: LocalDateTime? = null,
    val lcid: Int = 0,
    var roles: List<CoreRole> = listOf(CoreRole.END_USER),
    var groups: List<CoreGroup> = listOf(CoreGroup(users = emptyList(), communities = communities).save())
) : Entity() {
    fun save(): CoreUser {
        MovensDb.core().use { connection ->
            id = connection.prepareStatement(
                """
                INSERT INTO "Users" 
                ("UserName", "Email", "CreatedDate", "Guid", "LastPasswordChangeTimestamp", "Password", "LCID", "IsConfirmed", "IsDisabled", "Surname", "DeletionDate", "MobilePhoneNumber") 
                VALUES (?,?, '2019-01-01', ?, '2019-01-01', '', ?, true, false, ?, ?, ?)
                """, Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setString(1, username)
                it.setObject(2, email)
                it.setObject(3, guid)
                it.setObject(4, lcid)
                it.setObject(5, fullName)
                it.setObject(6, deletionDate)
                it.setObject(7, phoneNumber)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)

            communities.forEach {
                CoreJoinedCommunity(this, it).save()
                it.add(this)
            }

            groups.forEach {
                CoreUserGroup(this, it).save()
                it.add(this)
            }

            roles.forEach {
                CoreUserRole(this, it).save()
            }
        }
        return this
    }

    fun add(coreCommunity: CoreCommunity) {
        communities = communities.plus(coreCommunity)
    }

    fun add(group: CoreGroup) {
        groups = groups.plus(group)
    }
}

