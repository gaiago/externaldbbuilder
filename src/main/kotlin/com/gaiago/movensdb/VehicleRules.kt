package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS

class VehicleRules(
    val group: VehicleGroup = VehicleGroup(communities = emptyList()).save(),
    var zones: List<VehicleZone> = listOf(VehicleZone(rules = emptyList()).save()),
    val type: Int = ALLOW,
    val dayOfWeekStart: Int? = null,
    val dayOfWeekEnd: Int? = null,
    val minuteOfDayStart: Int? = null,
    val minuteOfDayEnd: Int? = null
) : Entity() {
    companion object {
        const val ALLOW = 0
        const val DENY = 1
    }

    fun save(): VehicleRules {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "Rules" 
                   ("CreatedDate", "LastUpdated", "GroupID", "Type", "DayOfWeekStart", "DayOfWeekEnd", "MinuteOfDayStart", "MinuteOfDayEnd") VALUES 
                   ('2019-01-01', '2019-01-01', ?, ?, ?, ?, ?, ?)""", RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, group.id)
                it.setInt(2, type)
                it.setObject(3, dayOfWeekStart)
                it.setObject(4, dayOfWeekEnd)
                it.setObject(5, minuteOfDayStart)
                it.setObject(6, minuteOfDayEnd)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        zones.forEach {
            VehicleZoneRules(this, it).save()
            it.add(this)
        }

        return this
    }

    fun add(vehicleZone: VehicleZone) {
        zones = zones.plus(vehicleZone)
    }
}