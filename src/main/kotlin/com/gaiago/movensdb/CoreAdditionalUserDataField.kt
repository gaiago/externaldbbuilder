package com.gaiago.movensdb

import com.gaiago.movensdb.CoreAdditionalUserDataType.STRING
import java.sql.Statement.RETURN_GENERATED_KEYS

class CoreAdditionalUserDataField(
        val name: String,
        val type: CoreAdditionalUserDataType = STRING,
        val required: Boolean = false
) : Entity() {
    fun save(): CoreAdditionalUserDataField {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                    """INSERT INTO "AdditionalUserDataFields" 
                        ("CreatedDate", "LastUpdated", "FieldName", "Type", "IsRequired") VALUES 
                        ('2019-01-01', '2019-01-01', ?, ?, ?) """,
                    RETURN_GENERATED_KEYS
            ).also {
                it.setString(1, name)
                it.setInt(2, type.code)
                it.setBoolean(3, required)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}
