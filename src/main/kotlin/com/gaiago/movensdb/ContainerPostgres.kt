package com.gaiago.movensdb

import org.testcontainers.containers.FixedHostPortGenericContainer
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy
import org.testcontainers.utility.MountableFile.forClasspathResource

class ContainerPostgres : FixedHostPortGenericContainer<Nothing>("postgres:11") {
    init {
        withEnv("POSTGRES_PASSWORD", "pg_password")
        withCopyFileToContainer(forClasspathResource("movensdb"), "/docker-entrypoint-initdb.d")
        withFixedExposedPort(5432, 5432)
        withReuse(true)
        waitingFor(waitStrategy())
    }

    private fun waitStrategy() = LogMessageWaitStrategy()
        .withRegEx(".*database system is ready to accept connections.*\\s")
        .withTimes(2)

    companion object {
        @JvmStatic
        private var CONTAINER: ContainerPostgres? = null

        @JvmStatic
        fun instance() {
            if (CONTAINER == null) {
                CONTAINER = ContainerPostgres()
                CONTAINER!!.start()
            }
            CONTAINER
        }
    }
}