package com.gaiago.movensdb

class VehicleZoneRules(
    private val vehicleRule: VehicleRules,
    private val vehicleZone: VehicleZone
) {
    fun save() {
        MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "ZoneRules" ("ZoneID", "RuleID") VALUES (?, ?)"""
            ).also {
                it.setInt(1, vehicleZone.id)
                it.setInt(2, vehicleRule.id)
                it.executeUpdate()
            }
        }
    }

}