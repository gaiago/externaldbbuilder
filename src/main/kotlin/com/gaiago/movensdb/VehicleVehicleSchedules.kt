package com.gaiago.movensdb

import java.sql.Statement
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.LocalDateTime.now

class VehicleVehicleSchedules(
    val vehicleId: Int,
    val startTimestamp: LocalDateTime,
    val endTimestamp: LocalDateTime? = null,
    val deletionDate: LocalDateTime? = null,
    val isVehicleAvailable: Boolean = false
) : Entity() {
    fun save(): VehicleVehicleSchedules {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "VehicleSchedules" ("VehicleID", "StartTimestamp", "EndTimestamp", "DeletionDate", "IsVehicleAvailable", "CreatedDate", "LastUpdated", "Type") VALUES (?, ?, ?, ?, ?, ?, ?, ?)""",
                Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, vehicleId)
                it.setTimestamp(2, Timestamp.valueOf(startTimestamp))
                it.setTimestamp(3, if (endTimestamp == null) null else Timestamp.valueOf(endTimestamp))
                it.setTimestamp(4, if (deletionDate == null) null else Timestamp.valueOf(deletionDate))
                it.setBoolean(5, isVehicleAvailable)
                it.setTimestamp(6, Timestamp.valueOf(now()))
                it.setTimestamp(7, Timestamp.valueOf(now()))
                it.setInt(8, 0)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}

