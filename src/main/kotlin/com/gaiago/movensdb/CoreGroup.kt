package com.gaiago.movensdb

import java.sql.Statement
import java.util.*

class CoreGroup(
    val guid: UUID = UUID.randomUUID(),
    var communities: List<CoreCommunity> = listOf(CoreCommunity(groups = emptyList(), users = emptyList()).save()),
    var users: List<CoreUser> = listOf(CoreUser(groups = emptyList(), communities = communities).save())
) : Entity() {
    fun save(): CoreGroup {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                """
                INSERT INTO "Groups" 
                ("Guid", "LastUpdated", "CreatedDate") 
                VALUES (?, '2019-01-01', '2019-01-01')
                """, Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setObject(1, guid)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        communities.forEach {
            CoreCommunityGroup(it, this).save()
            it.add(this)
        }

        users.forEach {
            CoreUserGroup(it, this).save()
            it.add(this)
        }

        return this
    }

    fun add(coreCommunity: CoreCommunity) {
        communities = communities.plus(coreCommunity)
    }

    fun add(coreUser: CoreUser) {
        users = users.plus(coreUser)
    }
}