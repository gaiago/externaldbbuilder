package com.gaiago.movensdb

import java.sql.Statement
import java.sql.Timestamp
import java.time.LocalDateTime

class VehicleMaintenanceOperations(
    val vehicleId: Int,
    val scheduleId: Int,
    val deletionDate: LocalDateTime? = null
) : Entity() {
    fun save(): VehicleMaintenanceOperations {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "MaintenanceOperations" ("VehicleID", "ScheduleID", "DeletionDate", "CreatedDate", "LastUpdated", "IsCompleted", "IsSuccessful") VALUES (?, ?, ?, ?, ?, ?, ?)""",
                Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, vehicleId)
                it.setInt(2, scheduleId)
                it.setTimestamp(3, if (deletionDate == null) null else Timestamp.valueOf(deletionDate))
                it.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()))
                it.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()))
                it.setBoolean(6, false)
                it.setBoolean(7, true)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}