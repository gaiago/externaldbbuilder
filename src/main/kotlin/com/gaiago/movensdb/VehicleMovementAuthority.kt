package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.util.*

class VehicleMovementAuthority(
    val guid: UUID = UUID.randomUUID(),
    val transportationRequestGuid: UUID = UUID.randomUUID(),
    val createdDate: LocalDateTime = now(),
    val startTime: LocalDateTime = now(),
    val endTime: LocalDateTime = now().plusHours(1),
    val deletionTime: LocalDateTime? = null,
    val confirmed: Boolean = true,
    val vehicle: VehicleVehicle? = null
) : Entity() {

    fun save(): VehicleMovementAuthority {
        MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """
                    INSERT INTO "MovementAuthorities" 
                    ("TransportRequest", "CreatedDate", "StartTime", "EndTime", "OriginatorModule", "LastUpdated", "Guid", "IsConfirmed", "IsReadyToStart", "MaxTripCount", "MinValidStartTime", "MaxValidEndTime", "MaxValidStartTime", "DeletionDate", "EquipmentReference", "AssignedVehicleID") 
                    VALUES (?, ?, ?, ?, ?, '2019-01-01', ?, ?, false, 1, ?, ?, ?, ?, ?, ?)
                    """, RETURN_GENERATED_KEYS
            ).also {
                it.setObject(1, transportationRequestGuid)
                it.setTimestamp(2, Timestamp.valueOf(createdDate))
                it.setTimestamp(3, Timestamp.valueOf(startTime))
                it.setTimestamp(4, Timestamp.valueOf(endTime))
                it.setObject(5, UUID.randomUUID())
                it.setObject(6, guid)
                it.setBoolean(7, confirmed)
                it.setTimestamp(8, Timestamp.valueOf(startTime.minusMinutes(12)))
                it.setTimestamp(9, Timestamp.valueOf(endTime.plusMinutes(1)))
                it.setTimestamp(10, Timestamp.valueOf(startTime.plusMinutes(15)))
                it.setTimestamp(11, if (deletionTime == null) null else Timestamp.valueOf(deletionTime))
                it.setString(12, if(vehicle == null) null else "${vehicle.plate};${vehicle.sticker ?: ""};${vehicle.vin ?: ""}")
                it.setObject(13, vehicle?.id)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}
