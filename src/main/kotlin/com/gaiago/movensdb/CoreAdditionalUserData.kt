package com.gaiago.movensdb

import java.sql.Statement

class CoreAdditionalUserData(
        val user: CoreUser,
        val field: CoreAdditionalUserDataField,
        val value: String
) : Entity() {
    fun save(): CoreAdditionalUserData {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                    """INSERT INTO "AdditionalUserData" 
                        ("UserID", "CreatedDate", "LastUpdated", "FieldName", "Type", "Value") VALUES 
                        (?, '2019-01-01', '2019-01-01', ?, ?, ?) """,
                    Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, user.id)
                it.setString(2, field.name)
                it.setInt(3, field.type.code)
                it.setString(4, value)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}