package com.gaiago.movensdb

import java.sql.Statement
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.util.*

class CoreMovementAuthority(
    val guid: UUID = UUID.randomUUID(),
    val createdDate: LocalDateTime = now(),
    val deletionTime: LocalDateTime? = null,
    val confirmed: Boolean = true,
    val equipmentReference: String? = "AA000ZZ;;",
    val transportationRequest: CoreTransportationRequest = CoreTransportationRequest().save()
) : Entity() {
    val user = transportationRequest.user
    val startTime = transportationRequest.startTime
    val endTime = transportationRequest.endTime
    val minStartTime = transportationRequest.startTime.minusMinutes(12)
    val maxStartTime = transportationRequest.startTime.plusMinutes(15)
    val maxEndTime = transportationRequest.endTime.plusMinutes(1)

    fun save(): CoreMovementAuthority {
        MovensDb.core().use { connection ->
            id = connection.prepareStatement(
                """
                    INSERT INTO "MovementAuthorities" 
                    ("TransportationRequestID", "CreatedDate", "StartTime", "EndTime", "OriginatorModule", "LastUpdated", "Guid", "IsConfirmed", "IsReadyToStart", "MaxTripCount", "MinValidStartTime", "MaxValidEndTime", "MaxValidStartTime", "DeletionDate", "EquipmentReference") 
                    VALUES (?, ?, ?, ?, ?, '2019-01-01', ?, ?, true, 1, ?, ?, ?, ?, ?)
                    """, Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, transportationRequest.id)
                it.setTimestamp(2, Timestamp.valueOf(createdDate))
                it.setTimestamp(3, Timestamp.valueOf(startTime))
                it.setTimestamp(4, Timestamp.valueOf(endTime))
                it.setObject(5, UUID.randomUUID())
                it.setObject(6, guid)
                it.setBoolean(7, confirmed)
                it.setTimestamp(8, Timestamp.valueOf(minStartTime))
                it.setTimestamp(9, Timestamp.valueOf(maxEndTime))
                it.setTimestamp(10, Timestamp.valueOf(maxStartTime))
                it.setTimestamp(11, if (deletionTime == null) null else Timestamp.valueOf(deletionTime))
                it.setString(12, equipmentReference)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }
        return this
    }

}
