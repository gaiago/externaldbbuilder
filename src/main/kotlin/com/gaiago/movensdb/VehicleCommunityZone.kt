package com.gaiago.movensdb

class VehicleCommunityZone(private val community: VehicleCommunity, private val zone: VehicleZone) {
    fun save(): VehicleCommunityZone {
        MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "CommunityZones" ("ZoneID", "CommunityID") VALUES (?, ?)"""
            ).also {
                it.setInt(1, zone.id)
                it.setInt(2, community.id)
            }.executeUpdate()
        }

        return this
    }
}