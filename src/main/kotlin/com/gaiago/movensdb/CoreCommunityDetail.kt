package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS

class CoreCommunityDetail(
    val community: CoreCommunity,
    var key: String,
    var value: String
) : Entity() {
    fun save(): CoreCommunityDetail {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "CommunityDetail" ("CommunityID", "LastUpdated", "Key", "Value") VALUES (?, '2019-01-01', ?, ?);""",
                RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, community.id)
                it.setString(2, key)
                it.setString(3, value)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}