package com.gaiago.movensdb

import java.sql.Statement
import java.util.*

class VehicleGarage(
    val name: String = "Underground Box",
    val address: String = "Street 55",
    val city: String = "Toronto",
    val notes: String = "Nothing",
    val latitude: Double = Random().nextDouble(),
    val longitude: Double = Random().nextDouble()
) : Entity() {
    fun save(): VehicleGarage {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement(
                """INSERT INTO "Garage" ("CreatedDate", "Name", "Address", "City", "Notes", "ReceptionLatitude", "ReceptionLongitude") VALUES ('2019-01-01', ?, ?, ?, ?, ?, ?)""",
                Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setString(1, name)
                it.setString(2, address)
                it.setString(3, city)
                it.setString(4, notes)
                it.setDouble(5, latitude)
                it.setDouble(6, longitude)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}

