package com.gaiago.movensdb

import java.sql.Statement
import java.sql.Timestamp.valueOf
import java.sql.Types
import java.time.LocalDateTime
import java.util.*
import java.util.UUID.randomUUID

class CoreLicenseDocuments(
    val user: CoreUser,
    val isValid: Boolean? = null,
    val guid: UUID = randomUUID(),
    val issuingAuthority: CoreIssuingAuthorities = CoreIssuingAuthorities().save(),
    val number: String = "AAA12",
    val issueDate: LocalDateTime = LocalDateTime.now().minusYears(5),
    val expiryDate: LocalDateTime? = LocalDateTime.now().plusYears(5),
    val attachments: List<UUID> = listOf(randomUUID())
) : Entity() {
    fun save(): CoreLicenseDocuments {
        MovensDb.core().use { connection ->
            id = connection.prepareStatement(
                """
                    INSERT INTO "LicenseDocuments" 
                    ("UserID", "IssuingAuthorityID", "CreatedDate", "LastUpdated", "LicenseNumber", "IssueDate", "IsValid", "Type", "Guid", "ExpiryDate", "Attachments") 
                    VALUES (?, ?, '2019-01-01', '2019-01-01', ?, ?, ?, 0, ?, ?, ?)
                    """, Statement.RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, user.id)
                it.setInt(2, issuingAuthority.id)
                it.setString(3, number)
                it.setTimestamp(4, valueOf(issueDate))
                it.setObject(5, isValid)
                it.setObject(6, guid)
                if (expiryDate != null) {
                    it.setObject(7, valueOf(expiryDate))
                } else {
                    it.setNull(7, Types.TIMESTAMP)
                }
                it.setObject(8, connection.createArrayOf("UUID", attachments.toTypedArray()))
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }
}