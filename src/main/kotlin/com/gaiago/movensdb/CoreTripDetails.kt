package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS
import java.util.*

class CoreTripDetails(
    val guid: UUID = UUID.randomUUID(),
    val trip: CoreTrips,
    val originatorKey: OriginatorKey,
    val value: String
) : Entity() {
    fun save(): CoreTripDetails {
        id = MovensDb.core().use { connection ->
            connection.prepareStatement(
                """
                INSERT INTO "TripDetails" ("TripID", "DeletionDate", "Guid", "CreatedDate", "OriginatorKey", "Labels", "ValueLabels", "Value", "ValueType") VALUES
                (?,	NULL, ?,	'2019-10-04 13:42:59',	?,	'',	NULL,	?,	0)
                """, RETURN_GENERATED_KEYS
            ).also {
                it.setInt(1, trip.id)
                it.setObject(2, guid)
                it.setString(3, originatorKey.value)
                it.setString(4, value)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        return this
    }

    enum class OriginatorKey(val value: String) {
        RESOURCE_GROUP("wk-res-groups"),
        AVERAGE_SPEED("vs-average-speed"),
        CATEGORY("wk-category"),
        RUNNING_TIME("vs-actual-running-time"),
        FUEL_LEVEL("vs-last-fuel-level"),
        PARKING_TIME("vs-actual-parking-time"),
        MILEAGE("vs-mileage")
    }
}
