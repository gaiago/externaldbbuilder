package com.gaiago.movensdb

class CoreJoinedCommunity(private val coreUser: CoreUser, private val coreCommunity: CoreCommunity) {
    fun save() {
        MovensDb.core().use { connection ->
            connection.prepareStatement("""INSERT INTO "JoinedCommunity" ("UserID", "CommunityID", "JoinTimestamp", "IsCommunityAdmin") VALUES (?, ?, '2019-01-01', false)""")
                .also {
                    it.setInt(1, coreUser.id)
                    it.setInt(2, coreCommunity.id)
                }.executeUpdate()
        }
    }
}
