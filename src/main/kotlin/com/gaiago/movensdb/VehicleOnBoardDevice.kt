package com.gaiago.movensdb

import java.sql.Statement.RETURN_GENERATED_KEYS
import java.util.*

class VehicleOnBoardDevice(
    val vehicle: VehicleVehicle? = VehicleVehicle().save(),
    val guid: UUID = UUID.randomUUID(),
    val externalGuid: UUID = UUID.randomUUID(),
    val driver: UUID = UUID.randomUUID(),
    val serialNumber: String = Random().nextInt().toString(),
    val hardwareAddress: String? = Random().nextInt().toString(),
    val manufacturer: String = "Dummy Device Corp.",
    val modelMain: String = "Dummy Box",
    val isEnabled: Boolean = true,
    val community: VehicleCommunity = VehicleCommunity().save()
) : Entity() {
    fun save() : VehicleOnBoardDevice {
        id = MovensDb.vehicle().use { connection ->
            connection.prepareStatement("""INSERT INTO "OnBoardDevices" 
                    ("VehicleID", "CreatedDate", "Guid", "ExternalGuid", "Driver", "IsEnabled", "SerialNumber", "HardwareAddress", "Manufacturer", "ModelMain", "CommunityID")
                    VALUES (?, '2019-01-01', ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """, RETURN_GENERATED_KEYS
            ).also {
                it.setObject(1, vehicle?.id)
                it.setObject(2, guid)
                it.setObject(3, externalGuid)
                it.setObject(4, driver)
                it.setBoolean(5, isEnabled)
                it.setString(6, serialNumber)
                it.setString(7, hardwareAddress)
                it.setString(8, manufacturer)
                it.setString(9, modelMain)
                it.setInt(10, community.id)
                it.executeUpdate()
            }.generatedKeys.also { it.next() }.getInt(1)
        }

        vehicle?.update(this)

        return this
    }
}
