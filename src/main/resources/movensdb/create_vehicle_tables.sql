DROP DATABASE IF EXISTS movens_vehicle;
CREATE DATABASE movens_vehicle;

\connect "movens_vehicle";

-- public."Attachments" definition

-- Drop table

-- DROP TABLE public."Attachments";

CREATE TABLE public."Attachments" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"FileName" varchar(200) NOT NULL,
	"StoragePath" varchar(500) NULL,
	"MimeType" varchar(300) NULL,
	"Type" int4 NOT NULL,
	"Size" int8 NOT NULL,
	"IsDataCommitted" bool NOT NULL,
	"Guid" uuid NOT NULL,
	CONSTRAINT "PK_Attachments" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Attachments_FileName" ON public."Attachments" USING btree ("FileName");
CREATE UNIQUE INDEX "IX_Attachments_Guid" ON public."Attachments" USING btree ("Guid");
CREATE INDEX "IX_Attachments_ID" ON public."Attachments" USING btree ("ID");


-- public."DriverSettings" definition

-- Drop table

-- DROP TABLE public."DriverSettings";

CREATE TABLE public."DriverSettings" (
	"ID" serial NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Driver" uuid NOT NULL,
	"Name" varchar(150) NULL,
	"Value" varchar(4100) NULL,
	CONSTRAINT "PK_DriverSettings" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_DriverSettings_ID" ON public."DriverSettings" USING btree ("ID");


-- public."Facts" definition

-- Drop table

-- DROP TABLE public."Facts";

CREATE TABLE public."Facts" (
	"ID" serial NOT NULL,
	"Reference" timestamp NOT NULL,
	"EntityType" varchar(250) NULL,
	"EntityID" int4 NULL,
	"EntityGuid" uuid NULL,
	"Type" int4 NOT NULL,
	"PerformingUsername" varchar(250) NULL,
	"AdditionalData1" varchar(250) NULL,
	"AdditionalData2" varchar(250) NULL,
	"PreviousFactReference" timestamp NULL,
	"PerformingGuid" uuid NULL,
	CONSTRAINT "PK_Facts" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Facts_ID" ON public."Facts" USING btree ("ID");


-- public."Filters" definition

-- Drop table

-- DROP TABLE public."Filters";

CREATE TABLE public."Filters" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"ValueType" int4 NOT NULL,
	"Key" varchar(100) NULL,
	"Guid" uuid NOT NULL,
	"Labels" varchar(2048) NULL,
	"ValidValues" varchar(2048) NULL,
	"ValidValuesLabels" varchar(2097152) NULL,
	"IsRequired" bool NOT NULL,
	CONSTRAINT "PK_Filters" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Filters_ID" ON public."Filters" USING btree ("ID");
CREATE UNIQUE INDEX "IX_Filters_Key" ON public."Filters" USING btree ("Key");


-- public."Garage" definition

-- Drop table

-- DROP TABLE public."Garage";

CREATE TABLE public."Garage" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"Name" varchar(100) NULL,
	"Address" varchar(250) NULL,
	"City" varchar(100) NULL,
	"Notes" varchar(250) NULL,
	"Contacts" varchar(250) NULL,
	"ReceptionLatitude" float8 NULL,
	"ReceptionLongitude" float8 NULL,
	CONSTRAINT "PK_Garage" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Garage_ID" ON public."Garage" USING btree ("ID");


-- public."Groups" definition

-- Drop table

-- DROP TABLE public."Groups";

CREATE TABLE public."Groups" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Guid" uuid NOT NULL,
	"Name" varchar(100) NULL,
	"Description" varchar(1024) NULL,
	CONSTRAINT "PK_Groups" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Groups_ID" ON public."Groups" USING btree ("ID");


-- public."MaintenanceOperationTypes" definition

-- Drop table

-- DROP TABLE public."MaintenanceOperationTypes";

CREATE TABLE public."MaintenanceOperationTypes" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"NameLabels" varchar(2048) NULL,
	"Name" varchar(150) NULL,
	CONSTRAINT "PK_MaintenanceOperationTypes" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_MaintenanceOperationTypes_ID" ON public."MaintenanceOperationTypes" USING btree ("ID");
CREATE UNIQUE INDEX "IX_MaintenanceOperationTypes_Name" ON public."MaintenanceOperationTypes" USING btree ("Name");


-- public."Modules" definition

-- Drop table

-- DROP TABLE public."Modules";

CREATE TABLE public."Modules" (
	"ID" serial NOT NULL,
	"Name" varchar(150) NULL,
	"LastAnnounced" timestamp NOT NULL,
	"Version" varchar(20) NULL,
	"RingId" varchar(100) NULL,
	CONSTRAINT "PK_Modules" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Modules_ID" ON public."Modules" USING btree ("ID");


-- public."PointsOfInterest" definition

-- Drop table

-- DROP TABLE public."PointsOfInterest";

CREATE TABLE public."PointsOfInterest" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Name" varchar(100) NULL,
	"Guid" uuid NOT NULL,
	"ExternalKey" varchar(100) NULL,
	"Address" varchar(250) NULL,
	"City" varchar(100) NULL,
	"Description" varchar(250) NULL,
	"ReceptionLatitude" float8 NULL,
	"ReceptionLongitude" float8 NULL,
	"CanRefuelElectricVehicles" bool NOT NULL,
	"CanRefuelICEVehicles" bool NOT NULL,
	"Type" int4 NOT NULL,
	CONSTRAINT "PK_PointsOfInterest" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_PointsOfInterest_ID" ON public."PointsOfInterest" USING btree ("ID");


-- public."VehicleOptionals" definition

-- Drop table

-- DROP TABLE public."VehicleOptionals";

CREATE TABLE public."VehicleOptionals" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"Key" varchar(150) NULL,
	"Labels" varchar(2048) NULL,
	"Guid" uuid NOT NULL,
	CONSTRAINT "PK_VehicleOptionals" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_VehicleOptionals_ID" ON public."VehicleOptionals" USING btree ("ID");


-- public."Zones" definition

-- Drop table

-- DROP TABLE public."Zones";

CREATE TABLE public."Zones" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Name" varchar(100) NULL,
	"MainEntranceAddress" varchar(250) NULL,
	"MainEntranceCity" varchar(100) NULL,
	"MainEntranceLatitude" float8 NULL,
	"MainEntranceLongitude" float8 NULL,
	"CentroidLatitude" float8 NULL,
	"CentroidLongitude" float8 NULL,
	"Geometry" bytea NULL,
	"MainEntranceOpeningHours" varchar(250) NULL,
	"MainEntranceTermsConditions" varchar(250) NULL,
	"IsPaidParking" bool NOT NULL,
	"CanRefuelElectricVehicles" bool NOT NULL,
	"CanRefuelICEVehicles" bool NOT NULL,
	"IsFreefloatingArea" bool NOT NULL,
	"IsRoundtripArea" bool NOT NULL,
	"IsAvailable" bool NOT NULL,
	"ParkingSpots" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"Type" int4 NOT NULL DEFAULT 0,
	CONSTRAINT "PK_Zones" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Zones_ID" ON public."Zones" USING btree ("ID");


-- public."__EFMigrationsHistory" definition

-- Drop table

-- DROP TABLE public."__EFMigrationsHistory";

CREATE TABLE public."__EFMigrationsHistory" (
	"MigrationId" varchar(150) NOT NULL,
	"ProductVersion" varchar(32) NOT NULL,
	CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);


-- public."Communities" definition

-- Drop table

-- DROP TABLE public."Communities";

CREATE TABLE public."Communities" (
	"ID" serial NOT NULL,
	"ParentID" int4 NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Name" varchar(250) NULL,
	"Level" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	CONSTRAINT "PK_Communities" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Communities_Communities_ParentID" FOREIGN KEY ("ParentID") REFERENCES "Communities"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_Communities_ID" ON public."Communities" USING btree ("ID");
CREATE INDEX "IX_Communities_ParentID" ON public."Communities" USING btree ("ParentID");


-- public."CommunityGroups" definition

-- Drop table

-- DROP TABLE public."CommunityGroups";

CREATE TABLE public."CommunityGroups" (
	"GroupID" int4 NOT NULL,
	"CommunityID" int4 NOT NULL,
	CONSTRAINT "PK_CommunityGroups" PRIMARY KEY ("CommunityID", "GroupID"),
	CONSTRAINT "FK_CommunityGroups_Communities_CommunityID" FOREIGN KEY ("CommunityID") REFERENCES "Communities"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_CommunityGroups_Groups_GroupID" FOREIGN KEY ("GroupID") REFERENCES "Groups"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_CommunityGroups_GroupID" ON public."CommunityGroups" USING btree ("GroupID");


-- public."CommunityZones" definition

-- Drop table

-- DROP TABLE public."CommunityZones";

CREATE TABLE public."CommunityZones" (
	"ZoneID" int4 NOT NULL,
	"CommunityID" int4 NOT NULL,
	CONSTRAINT "PK_CommunityZones" PRIMARY KEY ("CommunityID", "ZoneID"),
	CONSTRAINT "FK_CommunityZones_Communities_CommunityID" FOREIGN KEY ("CommunityID") REFERENCES "Communities"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_CommunityZones_Zones_ZoneID" FOREIGN KEY ("ZoneID") REFERENCES "Zones"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_CommunityZones_ZoneID" ON public."CommunityZones" USING btree ("ZoneID");


-- public."Roles" definition

-- Drop table

-- DROP TABLE public."Roles";

CREATE TABLE public."Roles" (
	"ID" serial NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Name" varchar(250) NULL,
	"Slug" varchar(50) NULL,
	"IsBackend" bool NOT NULL,
	"IsFrontend" bool NOT NULL,
	"IsOperator" bool NOT NULL,
	"ModuleID" int4 NULL,
	CONSTRAINT "PK_Roles" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Roles_Modules_ModuleID" FOREIGN KEY ("ModuleID") REFERENCES "Modules"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_Roles_ID" ON public."Roles" USING btree ("ID");
CREATE INDEX "IX_Roles_ModuleID" ON public."Roles" USING btree ("ModuleID");


-- public."Rules" definition

-- Drop table

-- DROP TABLE public."Rules";

CREATE TABLE public."Rules" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"GroupID" int4 NOT NULL,
	"Type" int4 NOT NULL,
	"DayOfWeekStart" int4 NULL,
	"DayOfWeekEnd" int4 NULL,
	"MinuteOfDayStart" int4 NULL,
	"MinuteOfDayEnd" int4 NULL,
	"DayOfMonthStart" int4 NULL,
	"DayOfMonthEnd" int4 NULL,
	"DayStart" timestamp NULL,
	"DayEnd" timestamp NULL,
	CONSTRAINT "PK_Rules" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Rules_Groups_GroupID" FOREIGN KEY ("GroupID") REFERENCES "Groups"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_Rules_GroupID" ON public."Rules" USING btree ("GroupID");
CREATE INDEX "IX_Rules_ID" ON public."Rules" USING btree ("ID");


-- public."VehicleCategories" definition

-- Drop table

-- DROP TABLE public."VehicleCategories";

CREATE TABLE public."VehicleCategories" (
	"ID" serial NOT NULL,
	"ParentID" int4 NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"Labels" varchar(2048) NULL,
	"Manufacturer" varchar(100) NULL,
	"Model" varchar(100) NULL,
	"Guid" uuid NOT NULL,
	"Year" int4 NULL,
	"PassengerSeats" int4 NULL,
	"Image" bytea NULL,
	"ImageThumbnail" bytea NULL,
	CONSTRAINT "PK_VehicleCategories" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_VehicleCategories_VehicleCategories_ParentID" FOREIGN KEY ("ParentID") REFERENCES "VehicleCategories"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_VehicleCategories_ID" ON public."VehicleCategories" USING btree ("ID");
CREATE INDEX "IX_VehicleCategories_ParentID" ON public."VehicleCategories" USING btree ("ParentID");


-- public."ZoneRules" definition

-- Drop table

-- DROP TABLE public."ZoneRules";

CREATE TABLE public."ZoneRules" (
	"ZoneID" int4 NOT NULL,
	"RuleID" int4 NOT NULL,
	CONSTRAINT "PK_ZoneRules" PRIMARY KEY ("ZoneID", "RuleID"),
	CONSTRAINT "FK_ZoneRules_Rules_RuleID" FOREIGN KEY ("RuleID") REFERENCES "Rules"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_ZoneRules_Zones_ZoneID" FOREIGN KEY ("ZoneID") REFERENCES "Zones"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_ZoneRules_RuleID" ON public."ZoneRules" USING btree ("RuleID");


-- public."Vehicle" definition

-- Drop table

-- DROP TABLE public."Vehicle";

CREATE TABLE public."Vehicle" (
	"ID" serial NOT NULL,
	"CategoryID" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"NominalGarageID" int4 NOT NULL,
	"RecoveringGarageID" int4 NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"LicensePlate" varchar(15) NULL,
	"VIN" varchar(30) NULL,
	"Sticker" varchar(30) NULL,
	"Latitude" float8 NULL,
	"Longitude" float8 NULL,
	"LastPositionUpdate" timestamp NULL,
	"OdometerKm" float8 NULL,
	"FuelLevelPct" float8 NULL,
	"LastSetOperativeBy" varchar(150) NULL,
	"LastSetOperativeById" uuid NOT NULL,
	"SystemOperativeStatus" bool NOT NULL,
	"Notes" varchar(2048) NULL,
	"AdminOperativeStatus" bool NOT NULL DEFAULT false,
	"SystemOperativeReason" varchar(250) NULL,
	"OverriddenSystemOperativeStatus" bool NOT NULL DEFAULT false,
	"ExternalCleaningStatus" int4 NULL,
	"InternalCleaningStatus" int4 NULL,
	CONSTRAINT "PK_Vehicle" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Vehicle_Garage_NominalGarageID" FOREIGN KEY ("NominalGarageID") REFERENCES "Garage"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_Vehicle_Garage_RecoveringGarageID" FOREIGN KEY ("RecoveringGarageID") REFERENCES "Garage"("ID") ON DELETE RESTRICT,
	CONSTRAINT "FK_Vehicle_VehicleCategories_CategoryID" FOREIGN KEY ("CategoryID") REFERENCES "VehicleCategories"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_Vehicle_CategoryID" ON public."Vehicle" USING btree ("CategoryID");
CREATE INDEX "IX_Vehicle_ID" ON public."Vehicle" USING btree ("ID");
CREATE INDEX "IX_Vehicle_NominalGarageID" ON public."Vehicle" USING btree ("NominalGarageID");
CREATE INDEX "IX_Vehicle_RecoveringGarageID" ON public."Vehicle" USING btree ("RecoveringGarageID");


-- public."VehicleGroup" definition

-- Drop table

-- DROP TABLE public."VehicleGroup";

CREATE TABLE public."VehicleGroup" (
	"VehicleID" int4 NOT NULL,
	"GroupID" int4 NOT NULL,
	CONSTRAINT "PK_VehicleGroup" PRIMARY KEY ("VehicleID", "GroupID"),
	CONSTRAINT "FK_VehicleGroup_Groups_GroupID" FOREIGN KEY ("GroupID") REFERENCES "Groups"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_VehicleGroup_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_VehicleGroup_GroupID" ON public."VehicleGroup" USING btree ("GroupID");


-- public."VehicleLogs" definition

-- Drop table

-- DROP TABLE public."VehicleLogs";

CREATE TABLE public."VehicleLogs" (
	"ID" serial NOT NULL,
	"VehicleID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"AdditionalData" varchar(4100) NULL,
	"Description" varchar(150) NULL,
	"EventType" int4 NOT NULL,
	"Reference" timestamp NOT NULL,
	"EnteredBy" varchar(150) NULL,
	"EnteredById" uuid NULL,
	CONSTRAINT "PK_VehicleLogs" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_VehicleLogs_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_VehicleLogs_ID" ON public."VehicleLogs" USING btree ("ID");
CREATE INDEX "IX_VehicleLogs_VehicleID" ON public."VehicleLogs" USING btree ("VehicleID");


-- public."VehicleMetrics" definition

-- Drop table

-- DROP TABLE public."VehicleMetrics";

CREATE TABLE public."VehicleMetrics" (
	"ID" serial NOT NULL,
	"VehicleID" int4 NOT NULL,
	"Name" uuid NOT NULL,
	"Start" timestamp NOT NULL,
	"MinValue" float8 NULL,
	"MaxValue" float8 NULL,
	"FirstValue" float8 NULL,
	"LastValue" float8 NULL,
	"Average" float8 NULL,
	"Data" bytea NULL,
	CONSTRAINT "PK_VehicleMetrics" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_VehicleMetrics_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_VehicleMetrics_ID" ON public."VehicleMetrics" USING btree ("ID");
CREATE INDEX "IX_VehicleMetrics_Name" ON public."VehicleMetrics" USING btree ("Name");
CREATE INDEX "IX_VehicleMetrics_Start" ON public."VehicleMetrics" USING btree ("Start");
CREATE INDEX "IX_VehicleMetrics_VehicleID" ON public."VehicleMetrics" USING btree ("VehicleID");


-- public."VehicleOptionalMatch" definition

-- Drop table

-- DROP TABLE public."VehicleOptionalMatch";

CREATE TABLE public."VehicleOptionalMatch" (
	"VehicleOptionalID" int4 NOT NULL,
	"VehicleID" int4 NOT NULL,
	"Quantity" int4 NOT NULL,
	CONSTRAINT "PK_VehicleOptionalMatch" PRIMARY KEY ("VehicleOptionalID", "VehicleID"),
	CONSTRAINT "FK_VehicleOptionalMatch_VehicleOptionals_VehicleOptionalID" FOREIGN KEY ("VehicleOptionalID") REFERENCES "VehicleOptionals"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_VehicleOptionalMatch_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_VehicleOptionalMatch_VehicleID" ON public."VehicleOptionalMatch" USING btree ("VehicleID");


-- public."VehicleSchedules" definition

-- Drop table

-- DROP TABLE public."VehicleSchedules";

CREATE TABLE public."VehicleSchedules" (
	"ID" serial NOT NULL,
	"VehicleID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"StartTimestamp" timestamp NOT NULL,
	"EndTimestamp" timestamp NULL,
	"IsVehicleAvailable" bool NOT NULL,
	"Type" int4 NOT NULL,
	CONSTRAINT "PK_VehicleSchedules" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_VehicleSchedules_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_VehicleSchedules_ID" ON public."VehicleSchedules" USING btree ("ID");
CREATE INDEX "IX_VehicleSchedules_VehicleID" ON public."VehicleSchedules" USING btree ("VehicleID");


-- public."VehicleTrips" definition

-- Drop table

-- DROP TABLE public."VehicleTrips";

CREATE TABLE public."VehicleTrips" (
	"ID" serial NOT NULL,
	"VehicleID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"StartLongitude" float8 NULL,
	"StartLatitude" float8 NULL,
	"EndLongitude" float8 NULL,
	"EndLatitude" float8 NULL,
	"IsCompleted" bool NOT NULL,
	"TripType" int4 NOT NULL,
	"StartTimestamp" timestamp NOT NULL,
	"EndTimestamp" timestamp NULL,
	"OriginatingDeviceKey" varchar(150) NULL,
	CONSTRAINT "PK_VehicleTrips" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_VehicleTrips_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_VehicleTrips_ID" ON public."VehicleTrips" USING btree ("ID");
CREATE INDEX "IX_VehicleTrips_VehicleID" ON public."VehicleTrips" USING btree ("VehicleID");


-- public."MaintenanceOperations" definition

-- Drop table

-- DROP TABLE public."MaintenanceOperations";

CREATE TABLE public."MaintenanceOperations" (
	"ID" serial NOT NULL,
	"OperatingBaseID" int4 NULL,
	"VehicleID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Description" varchar(4096) NULL,
	"Summary" varchar(250) NULL,
	"EnteredBy" varchar(250) NULL,
	"SignedOffBy" varchar(250) NULL,
	"PerformedBy" varchar(250) NULL,
	"PerformanceBegin" timestamp NULL,
	"PerformanceEnd" timestamp NULL,
	"IsCompleted" bool NOT NULL,
	"IsSuccessful" bool NOT NULL,
	"ScheduleID" int4 NOT NULL DEFAULT 0,
	CONSTRAINT "PK_MaintenanceOperations" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_MaintenanceOperations_Garage_OperatingBaseID" FOREIGN KEY ("OperatingBaseID") REFERENCES "Garage"("ID") ON DELETE RESTRICT,
	CONSTRAINT "FK_MaintenanceOperations_VehicleSchedules_ScheduleID" FOREIGN KEY ("ScheduleID") REFERENCES "VehicleSchedules"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_MaintenanceOperations_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_MaintenanceOperations_ID" ON public."MaintenanceOperations" USING btree ("ID");
CREATE INDEX "IX_MaintenanceOperations_OperatingBaseID" ON public."MaintenanceOperations" USING btree ("OperatingBaseID");
CREATE INDEX "IX_MaintenanceOperations_ScheduleID" ON public."MaintenanceOperations" USING btree ("ScheduleID");
CREATE INDEX "IX_MaintenanceOperations_VehicleID" ON public."MaintenanceOperations" USING btree ("VehicleID");


-- public."MovementAuthorities" definition

-- Drop table

-- DROP TABLE public."MovementAuthorities";

CREATE TABLE public."MovementAuthorities" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"AssignedVehicleID" int4 NULL,
	"StartTime" timestamp NOT NULL,
	"EndTime" timestamp NOT NULL,
	"StartZoneID" int4 NULL,
	"EndZoneID" int4 NULL,
	"StartAddressOverride" text NULL,
	"StartLatitudeOverride" float8 NULL,
	"StartLongitudeOverride" float8 NULL,
	"OriginatorKey" varchar(100) NULL,
	"OriginatorModule" uuid NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"TransportRequest" uuid NOT NULL,
	"Guid" uuid NOT NULL,
	"IsConfirmed" bool NOT NULL,
	"IsReadyToStart" bool NOT NULL,
	"MaxTripCount" int4 NOT NULL,
	"MinValidStartTime" timestamp NOT NULL,
	"MaxValidEndTime" timestamp NOT NULL,
	"MaxValidStartTime" timestamp NOT NULL,
	"EquipmentReference" varchar(150) NULL,
	"ServiceReference" varchar(150) NULL,
	CONSTRAINT "PK_MovementAuthorities" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_MovementAuthorities_Vehicle_AssignedVehicleID" FOREIGN KEY ("AssignedVehicleID") REFERENCES "Vehicle"("ID") ON DELETE RESTRICT,
	CONSTRAINT "FK_MovementAuthorities_Zones_EndZoneID" FOREIGN KEY ("EndZoneID") REFERENCES "Zones"("ID") ON DELETE RESTRICT,
	CONSTRAINT "FK_MovementAuthorities_Zones_StartZoneID" FOREIGN KEY ("StartZoneID") REFERENCES "Zones"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_MovementAuthorities_AssignedVehicleID" ON public."MovementAuthorities" USING btree ("AssignedVehicleID");
CREATE INDEX "IX_MovementAuthorities_EndZoneID" ON public."MovementAuthorities" USING btree ("EndZoneID");
CREATE UNIQUE INDEX "IX_MovementAuthorities_Guid" ON public."MovementAuthorities" USING btree ("Guid");
CREATE INDEX "IX_MovementAuthorities_ID" ON public."MovementAuthorities" USING btree ("ID");
CREATE INDEX "IX_MovementAuthorities_StartZoneID" ON public."MovementAuthorities" USING btree ("StartZoneID");


-- public."MovementDetails" definition

-- Drop table

-- DROP TABLE public."MovementDetails";

CREATE TABLE public."MovementDetails" (
	"ID" serial NOT NULL,
	"MovementAuthorityID" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"OriginatorKey" varchar(100) NULL,
	"Labels" varchar(2048) NULL,
	"ValueLabels" varchar(2048) NULL,
	"Value" varchar(1024) NULL,
	"ValueType" int4 NOT NULL,
	CONSTRAINT "PK_MovementDetails" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_MovementDetails_MovementAuthorities_MovementAuthorityID" FOREIGN KEY ("MovementAuthorityID") REFERENCES "MovementAuthorities"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_MovementDetails_ID" ON public."MovementDetails" USING btree ("ID");
CREATE INDEX "IX_MovementDetails_MovementAuthorityID" ON public."MovementDetails" USING btree ("MovementAuthorityID");


-- public."OnBoardDevices" definition

-- Drop table

-- DROP TABLE public."OnBoardDevices";

CREATE TABLE public."OnBoardDevices" (
	"ID" serial NOT NULL,
	"VehicleID" int4 NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"Manufacturer" varchar(150) NULL,
	"ModelMain" varchar(50) NULL,
	"ModelSubtype" varchar(50) NULL,
	"HardwareVersion" varchar(20) NULL,
	"PlatformVersion" varchar(20) NULL,
	"SoftwareVersion" varchar(20) NULL,
	"SerialNumber" varchar(25) NULL,
	"HardwareAddress" varchar(25) NULL,
	"ReferenceToken" varchar(25) NULL,
	"ExternalGuid" uuid NOT NULL,
	"Guid" uuid NOT NULL,
	"InstallationDate" timestamp NULL,
	"LastMaintenanceDate" timestamp NULL,
	"ExternalId" varchar(50) NULL,
	"LastEventReceived" timestamp NULL,
	"LastHFEventReceived" timestamp NULL,
	"LastSynchronization" timestamp NULL,
	"LastNetworkSignalStrengthPct" int4 NULL,
	"LastNetworkInfo" varchar(150) NULL,
	"Driver" uuid NOT NULL,
	"IsEnabled" bool NOT NULL,
	"CommunityID" int4 NULL,
	CONSTRAINT "PK_OnBoardDevices" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_OnBoardDevices_Communities_CommunityID" FOREIGN KEY ("CommunityID") REFERENCES "Communities"("ID") ON DELETE RESTRICT,
	CONSTRAINT "FK_OnBoardDevices_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_OnBoardDevices_CommunityID" ON public."OnBoardDevices" USING btree ("CommunityID");
CREATE INDEX "IX_OnBoardDevices_ID" ON public."OnBoardDevices" USING btree ("ID");
CREATE INDEX "IX_OnBoardDevices_VehicleID" ON public."OnBoardDevices" USING btree ("VehicleID");


-- public."Supplies" definition

-- Drop table

-- DROP TABLE public."Supplies";

CREATE TABLE public."Supplies" (
	"ID" serial NOT NULL,
	"VehicleID" int4 NOT NULL,
	"MeasurementUnit" int4 NOT NULL,
	"PercentValue" int4 NOT NULL,
	"ExternalKey" varchar(150) NULL,
	"Name" varchar(150) NULL,
	"IsPercentApproximated" bool NOT NULL,
	"RegisteredValue" float8 NULL,
	"MaxRegisteredValue" float8 NULL,
	"MinRegisteredValue" float8 NULL,
	"IsDepletableSupply" bool NOT NULL,
	"IsWarningLevel" bool NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	CONSTRAINT "PK_Supplies" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Supplies_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_Supplies_ID" ON public."Supplies" USING btree ("ID");
CREATE INDEX "IX_Supplies_VehicleID" ON public."Supplies" USING btree ("VehicleID");


-- public."VehicleTripLogs" definition

-- Drop table

-- DROP TABLE public."VehicleTripLogs";

CREATE TABLE public."VehicleTripLogs" (
	"ID" serial NOT NULL,
	"VehicleTripID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"Type" int4 NOT NULL,
	"AdditionalData" varchar(1024) NULL,
	"Source" int4 NOT NULL,
	"Reference" timestamp NOT NULL,
	CONSTRAINT "PK_VehicleTripLogs" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_VehicleTripLogs_VehicleTrips_VehicleTripID" FOREIGN KEY ("VehicleTripID") REFERENCES "VehicleTrips"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_VehicleTripLogs_ID" ON public."VehicleTripLogs" USING btree ("ID");
CREATE INDEX "IX_VehicleTripLogs_VehicleTripID" ON public."VehicleTripLogs" USING btree ("VehicleTripID");


-- public."BillableTrips" definition

-- Drop table

-- DROP TABLE public."BillableTrips";

CREATE TABLE public."BillableTrips" (
	"ID" serial NOT NULL,
	"VehicleTripID" int4 NULL,
	"MovementAuthorityID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Guid" uuid NOT NULL,
	"IsCompleted" bool NOT NULL,
	"IsBillable" bool NOT NULL,
	"BillableReason" varchar(250) NULL,
	"StartLocationName" varchar(250) NULL,
	"EndLocationName" varchar(250) NULL,
	"StartLocationLatitude" float8 NULL,
	"StartLocationLongitude" float8 NULL,
	"EndLocationLatitude" float8 NULL,
	"EndLocationLongitude" float8 NULL,
	"EndTimestamp" timestamp NULL,
	"StartTimestamp" timestamp NOT NULL,
	"Status" int4 NOT NULL,
	"Index" int4 NOT NULL,
	"EquipmentReference" varchar(150) NULL,
	"ServiceReference" varchar(150) NULL,
	"ActualDurationSeconds" int4 NOT NULL DEFAULT 0,
	"ActualMileageKm" int4 NOT NULL DEFAULT 0,
	CONSTRAINT "PK_BillableTrips" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_BillableTrips_MovementAuthorities_MovementAuthorityID" FOREIGN KEY ("MovementAuthorityID") REFERENCES "MovementAuthorities"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_BillableTrips_VehicleTrips_VehicleTripID" FOREIGN KEY ("VehicleTripID") REFERENCES "VehicleTrips"("ID") ON DELETE RESTRICT
);
CREATE UNIQUE INDEX "IX_BillableTrips_Guid" ON public."BillableTrips" USING btree ("Guid");
CREATE INDEX "IX_BillableTrips_ID" ON public."BillableTrips" USING btree ("ID");
CREATE INDEX "IX_BillableTrips_MovementAuthorityID" ON public."BillableTrips" USING btree ("MovementAuthorityID");
CREATE INDEX "IX_BillableTrips_VehicleTripID" ON public."BillableTrips" USING btree ("VehicleTripID");


-- public."DamageReports" definition

-- Drop table

-- DROP TABLE public."DamageReports";

CREATE TABLE public."DamageReports" (
	"ID" serial NOT NULL,
	"VehicleID" int4 NOT NULL,
	"LastMaintenanceOperationID" int4 NULL,
	"ReportingUser" uuid NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"Severity" int4 NOT NULL,
	"Solved" timestamp NULL,
	"SolvingUser" uuid NOT NULL,
	"Notes" varchar(2048) NULL,
	"Attachments" _uuid NULL,
	CONSTRAINT "PK_DamageReports" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_DamageReports_MaintenanceOperations_LastMaintenanceOperatio~" FOREIGN KEY ("LastMaintenanceOperationID") REFERENCES "MaintenanceOperations"("ID") ON DELETE RESTRICT,
	CONSTRAINT "FK_DamageReports_Vehicle_VehicleID" FOREIGN KEY ("VehicleID") REFERENCES "Vehicle"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_DamageReports_ID" ON public."DamageReports" USING btree ("ID");
CREATE INDEX "IX_DamageReports_LastMaintenanceOperationID" ON public."DamageReports" USING btree ("LastMaintenanceOperationID");
CREATE INDEX "IX_DamageReports_VehicleID" ON public."DamageReports" USING btree ("VehicleID");


-- public."DeviceActions" definition

-- Drop table

-- DROP TABLE public."DeviceActions";

CREATE TABLE public."DeviceActions" (
	"ID" serial NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"OnBoardDeviceID" int4 NOT NULL,
	"KeepTryingUntil" timestamp NULL,
	"Command" varchar(250) NULL,
	"Parameters" varchar(4100) NULL,
	"EnteredBy" varchar(150) NULL,
	"LastExecutionAttempt" timestamp NULL,
	"ShouldTryAgain" bool NOT NULL,
	"Response" varchar(4100) NULL,
	"LastAttemptId" varchar(150) NULL,
	"EnteredById" uuid NULL,
	"IsSuccessful" bool NULL,
	CONSTRAINT "PK_DeviceActions" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_DeviceActions_OnBoardDevices_OnBoardDeviceID" FOREIGN KEY ("OnBoardDeviceID") REFERENCES "OnBoardDevices"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_DeviceActions_ID" ON public."DeviceActions" USING btree ("ID");
CREATE INDEX "IX_DeviceActions_OnBoardDeviceID" ON public."DeviceActions" USING btree ("OnBoardDeviceID");


-- public."DeviceEvents" definition

-- Drop table

-- DROP TABLE public."DeviceEvents";

CREATE TABLE public."DeviceEvents" (
	"ID" serial NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"OnBoardDeviceID" int4 NOT NULL,
	"DeviceTimestamp" timestamp NOT NULL,
	"ExtEventId" varchar(150) NULL,
	"Type" varchar(150) NULL,
	"PropertyName" varchar(250) NULL,
	"PropertyValue" varchar(4100) NULL,
	"Source" varchar(150) NULL,
	"LastElaborationAttempt" timestamp NULL,
	"LastElaborationSuccessful" bool NOT NULL,
	CONSTRAINT "PK_DeviceEvents" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_DeviceEvents_OnBoardDevices_OnBoardDeviceID" FOREIGN KEY ("OnBoardDeviceID") REFERENCES "OnBoardDevices"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_DeviceEvents_ID" ON public."DeviceEvents" USING btree ("ID");
CREATE INDEX "IX_DeviceEvents_OnBoardDeviceID" ON public."DeviceEvents" USING btree ("OnBoardDeviceID");


-- public."DeviceFeedbacks" definition

-- Drop table

-- DROP TABLE public."DeviceFeedbacks";

CREATE TABLE public."DeviceFeedbacks" (
	"ID" serial NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"OnBoardDeviceID" int4 NOT NULL,
	"Timestamp" timestamp NOT NULL,
	"SmartphoneId" varchar(150) NULL,
	"LastAttemptResponse" varchar(4100) NULL,
	"LastAttempt" timestamp NULL,
	"LastAttemptSuccessful" bool NOT NULL,
	"LastAttemptId" varchar(150) NULL,
	"Payload" text NULL,
	CONSTRAINT "PK_DeviceFeedbacks" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_DeviceFeedbacks_OnBoardDevices_OnBoardDeviceID" FOREIGN KEY ("OnBoardDeviceID") REFERENCES "OnBoardDevices"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_DeviceFeedbacks_ID" ON public."DeviceFeedbacks" USING btree ("ID");
CREATE INDEX "IX_DeviceFeedbacks_OnBoardDeviceID" ON public."DeviceFeedbacks" USING btree ("OnBoardDeviceID");


-- public."DeviceSecrets" definition

-- Drop table

-- DROP TABLE public."DeviceSecrets";

CREATE TABLE public."DeviceSecrets" (
	"ID" serial NOT NULL,
	"DeviceID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"OwnerId" uuid NOT NULL,
	"DeviceId" uuid NOT NULL,
	"Data" varchar(2048) NULL,
	"ValidityStart" timestamp NULL,
	"ValidityEnd" timestamp NULL,
	CONSTRAINT "PK_DeviceSecrets" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_DeviceSecrets_OnBoardDevices_DeviceID" FOREIGN KEY ("DeviceID") REFERENCES "OnBoardDevices"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_DeviceSecrets_DeviceID" ON public."DeviceSecrets" USING btree ("DeviceID");
CREATE INDEX "IX_DeviceSecrets_ID" ON public."DeviceSecrets" USING btree ("ID");


-- public."MaintenanceOperationRows" definition

-- Drop table

-- DROP TABLE public."MaintenanceOperationRows";

CREATE TABLE public."MaintenanceOperationRows" (
	"ID" serial NOT NULL,
	"MaintenanceOperationID" int4 NOT NULL,
	"OperationTypeID" int4 NOT NULL,
	"DeviceID" int4 NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"PerformedBy" varchar(250) NULL,
	"Description" varchar(1024) NULL,
	CONSTRAINT "PK_MaintenanceOperationRows" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_MaintenanceOperationRows_MaintenanceOperationTypes_Operatio~" FOREIGN KEY ("OperationTypeID") REFERENCES "MaintenanceOperationTypes"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_MaintenanceOperationRows_MaintenanceOperations_MaintenanceO~" FOREIGN KEY ("MaintenanceOperationID") REFERENCES "MaintenanceOperations"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_MaintenanceOperationRows_OnBoardDevices_DeviceID" FOREIGN KEY ("DeviceID") REFERENCES "OnBoardDevices"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_MaintenanceOperationRows_DeviceID" ON public."MaintenanceOperationRows" USING btree ("DeviceID");
CREATE INDEX "IX_MaintenanceOperationRows_ID" ON public."MaintenanceOperationRows" USING btree ("ID");
CREATE INDEX "IX_MaintenanceOperationRows_MaintenanceOperationID" ON public."MaintenanceOperationRows" USING btree ("MaintenanceOperationID");
CREATE INDEX "IX_MaintenanceOperationRows_OperationTypeID" ON public."MaintenanceOperationRows" USING btree ("OperationTypeID");


-- public."BillableTripDetails" definition

-- Drop table

-- DROP TABLE public."BillableTripDetails";

CREATE TABLE public."BillableTripDetails" (
	"ID" serial NOT NULL,
	"BillableTripID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"Guid" uuid NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"OriginatorKey" varchar(100) NULL,
	"Labels" varchar(2048) NULL,
	"ValueLabels" varchar(2048) NULL,
	"Value" varchar(1024) NULL,
	"ValueType" int4 NOT NULL,
	CONSTRAINT "PK_BillableTripDetails" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_BillableTripDetails_BillableTrips_BillableTripID" FOREIGN KEY ("BillableTripID") REFERENCES "BillableTrips"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_BillableTripDetails_BillableTripID" ON public."BillableTripDetails" USING btree ("BillableTripID");
CREATE INDEX "IX_BillableTripDetails_ID" ON public."BillableTripDetails" USING btree ("ID");


-- public."BillableTripEvents" definition

-- Drop table

-- DROP TABLE public."BillableTripEvents";

CREATE TABLE public."BillableTripEvents" (
	"ID" serial NOT NULL,
	"BillableTripID" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"Timestamp" timestamp NOT NULL,
	"Latitude" float8 NULL,
	"Longitude" float8 NULL,
	"Source" int4 NOT NULL,
	"Type" int4 NOT NULL,
	"AdditionalData" varchar(1024) NULL,
	"Process" int4 NOT NULL DEFAULT 0,
	"OriginatorKey" uuid NULL,
	CONSTRAINT "PK_BillableTripEvents" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_BillableTripEvents_BillableTrips_BillableTripID" FOREIGN KEY ("BillableTripID") REFERENCES "BillableTrips"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_BillableTripEvents_BillableTripID" ON public."BillableTripEvents" USING btree ("BillableTripID");
CREATE INDEX "IX_BillableTripEvents_ID" ON public."BillableTripEvents" USING btree ("ID");