DROP DATABASE IF EXISTS movens;
CREATE DATABASE movens;

\connect "movens";

-- public."AdditionalUserDataFields" definition

-- Drop table

-- DROP TABLE public."AdditionalUserDataFields";

CREATE TABLE public."AdditionalUserDataFields" (
	"ID" serial NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"FieldName" varchar(50) NOT NULL,
	"Type" int4 NOT NULL,
	"FieldLabels" varchar(2048) NULL,
	"FieldDescriptionLabels" varchar(8192) NULL,
	"ValidValues" varchar(2048) NULL,
	"IsRequired" bool NOT NULL,
	"IsServerOnly" bool NOT NULL DEFAULT false,
	"ValidValueLabels" varchar(8192) NULL,
	CONSTRAINT "PK_AdditionalUserDataFields" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_AdditionalUserDataFields_ID" ON public."AdditionalUserDataFields" USING btree ("ID");


-- public."Attachments" definition

-- Drop table

-- DROP TABLE public."Attachments";

CREATE TABLE public."Attachments" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"FileName" varchar(200) NOT NULL,
	"StoragePath" varchar(500) NULL,
	"MimeType" varchar(300) NULL,
	"Type" int4 NOT NULL,
	"Size" int8 NOT NULL,
	"IsDataCommitted" bool NOT NULL,
	"Guid" uuid NOT NULL,
	CONSTRAINT "PK_Attachments" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Attachments_FileName" ON public."Attachments" USING btree ("FileName");
CREATE UNIQUE INDEX "IX_Attachments_Guid" ON public."Attachments" USING btree ("Guid");
CREATE INDEX "IX_Attachments_ID" ON public."Attachments" USING btree ("ID");


-- public."CommunityLevels" definition

-- Drop table

-- DROP TABLE public."CommunityLevels";

CREATE TABLE public."CommunityLevels" (
	"ID" serial NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Name" varchar(250) NULL,
	"Level" int4 NOT NULL,
	CONSTRAINT "PK_CommunityLevels" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_CommunityLevels_ID" ON public."CommunityLevels" USING btree ("ID");
CREATE UNIQUE INDEX "IX_CommunityLevels_Level" ON public."CommunityLevels" USING btree ("Level");


-- public."Facts" definition

-- Drop table

-- DROP TABLE public."Facts";

CREATE TABLE public."Facts" (
	"ID" serial NOT NULL,
	"Reference" timestamp NOT NULL,
	"EntityType" varchar(250) NULL,
	"EntityID" int4 NULL,
	"EntityGuid" uuid NULL,
	"Type" int4 NOT NULL,
	"PerformingUsername" varchar(250) NULL,
	"AdditionalData1" varchar(250) NULL,
	"AdditionalData2" varchar(250) NULL,
	"PerformingGuid" uuid NULL,
	"PreviousFactReference" timestamp NULL,
	CONSTRAINT "PK_Facts" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Facts_ID" ON public."Facts" USING btree ("ID");


-- public."Groups" definition

-- Drop table

-- DROP TABLE public."Groups";

CREATE TABLE public."Groups" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Guid" uuid NOT NULL,
	"Labels" varchar(2048) NULL,
	CONSTRAINT "PK_Groups" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Groups_ID" ON public."Groups" USING btree ("ID");


-- public."IssuingAuthorities" definition

-- Drop table

-- DROP TABLE public."IssuingAuthorities";

CREATE TABLE public."IssuingAuthorities" (
	"ID" serial NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"DeletionDate" timestamp NULL,
	"Name" varchar(250) NOT NULL,
	"Code" varchar(50) NOT NULL,
	"IsToBeVerified" bool NOT NULL,
	"CountryCode" varchar(10) NOT NULL,
	CONSTRAINT "PK_IssuingAuthorities" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_IssuingAuthorities_ID" ON public."IssuingAuthorities" USING btree ("ID");


-- public."MessageTemplates" definition

-- Drop table

-- DROP TABLE public."MessageTemplates";

CREATE TABLE public."MessageTemplates" (
	"ID" serial NOT NULL,
	"Format" int4 NOT NULL,
	"LCID" int4 NOT NULL,
	"Sender" varchar(150) NULL,
	"Subject" varchar(450) NULL,
	"Body" text NULL,
	"HTMLBody" text NULL,
	"Type" int4 NOT NULL,
	CONSTRAINT "PK_MessageTemplates" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_MessageTemplates_ID" ON public."MessageTemplates" USING btree ("ID");


-- public."Modules" definition

-- Drop table

-- DROP TABLE public."Modules";

CREATE TABLE public."Modules" (
	"ID" serial NOT NULL,
	"Name" varchar(150) NULL,
	"LastAnnounced" timestamp NOT NULL,
	"Version" varchar(20) NULL,
	"RingId" varchar(100) NULL,
	CONSTRAINT "PK_Modules" PRIMARY KEY ("ID")
);
CREATE INDEX "IX_Modules_ID" ON public."Modules" USING btree ("ID");
CREATE UNIQUE INDEX "IX_Modules_RingId" ON public."Modules" USING btree ("RingId");


-- public."Users" definition

-- Drop table

-- DROP TABLE public."Users";

CREATE TABLE public."Users" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"UserName" varchar(100) NOT NULL,
	"Guid" uuid NOT NULL,
	"LastPasswordChangeTimestamp" timestamp NOT NULL,
	"Password" varchar(100) NOT NULL,
	"Salt" varchar(100) NULL,
	"LastLogonTimestamp" timestamp NULL,
	"LCID" int4 NOT NULL,
	"ExternalLoginData" varchar(100) NULL,
	"IsConfirmed" bool NOT NULL,
	"IsDisabled" bool NOT NULL,
	"EmailChangePendingValue" varchar(100) NULL,
	"Email" varchar(100) NULL,
	"Surname" varchar(100) NULL,
	"Name" varchar(100) NULL,
	"ActionToken" varchar(100) NULL,
	"MobilePhoneNumber" varchar(30) NULL,
	"MobilePhoneNumberChangePendingValue" varchar(30) NULL,
	"TFASecret" varchar(50) NULL,
	"AvatarImage" bytea NULL,
	"FirstName" varchar(100) NULL,
	"LastName" varchar(100) NULL,
	"Address" varchar(200) NULL,
	"City" varchar(150) NULL,
	"PostCode" varchar(20) NULL,
	"CountryCode" varchar(10) NULL,
	"ManualApprovalDate" timestamp NULL,
	CONSTRAINT "PK_Users" PRIMARY KEY ("ID")
);
CREATE UNIQUE INDEX "IX_Users_Guid" ON public."Users" USING btree ("Guid");
CREATE INDEX "IX_Users_ID" ON public."Users" USING btree ("ID");
CREATE UNIQUE INDEX "IX_Users_UserName" ON public."Users" USING btree ("UserName");


-- public."__EFMigrationsHistory" definition

-- Drop table

-- DROP TABLE public."__EFMigrationsHistory";

CREATE TABLE public."__EFMigrationsHistory" (
	"MigrationId" varchar(150) NOT NULL,
	"ProductVersion" varchar(32) NOT NULL,
	CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);


-- public."AdditionalUserData" definition

-- Drop table

-- DROP TABLE public."AdditionalUserData";

CREATE TABLE public."AdditionalUserData" (
	"ID" serial NOT NULL,
	"UserID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"FieldName" varchar(50) NOT NULL,
	"Type" int4 NOT NULL,
	"Value" varchar(1024) NULL,
	CONSTRAINT "PK_AdditionalUserData" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_AdditionalUserData_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_AdditionalUserData_ID" ON public."AdditionalUserData" USING btree ("ID");
CREATE INDEX "IX_AdditionalUserData_UserID" ON public."AdditionalUserData" USING btree ("UserID");


-- public."AuthenticatedSessions" definition

-- Drop table

-- DROP TABLE public."AuthenticatedSessions";

CREATE TABLE public."AuthenticatedSessions" (
	"ID" serial NOT NULL,
	"UserID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Token" varchar(100) NULL,
	"SmartphoneBrand" varchar(50) NULL,
	"SmartphoneModel" varchar(50) NULL,
	"OSName" varchar(50) NULL,
	"OSVersion" varchar(20) NULL,
	"AppName" varchar(50) NULL,
	"AppVersion" varchar(20) NULL,
	CONSTRAINT "PK_AuthenticatedSessions" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_AuthenticatedSessions_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_AuthenticatedSessions_ID" ON public."AuthenticatedSessions" USING btree ("ID");
CREATE INDEX "IX_AuthenticatedSessions_UserID" ON public."AuthenticatedSessions" USING btree ("UserID");


-- public."Communities" definition

-- Drop table

-- DROP TABLE public."Communities";

CREATE TABLE public."Communities" (
	"ID" serial NOT NULL,
	"ParentID" int4 NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Name" varchar(250) NULL,
	"Level" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"RegistrationCode" varchar(50) NULL,
	"AccentColor" int4 NULL,
	"Logo" bytea NULL,
	"PrimaryColor" int4 NULL,
	CONSTRAINT "PK_Communities" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Communities_Communities_ParentID" FOREIGN KEY ("ParentID") REFERENCES "Communities"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_Communities_ID" ON public."Communities" USING btree ("ID");
CREATE INDEX "IX_Communities_ParentID" ON public."Communities" USING btree ("ParentID");


-- public."CommunityDetail" definition

-- Drop table

-- DROP TABLE public."CommunityDetail";

CREATE TABLE public."CommunityDetail" (
	"ID" serial NOT NULL,
	"CommunityID" int4 NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Key" varchar(250) NULL,
	"Value" varchar(2048) NULL,
	CONSTRAINT "PK_CommunityDetail" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_CommunityDetail_Communities_CommunityID" FOREIGN KEY ("CommunityID") REFERENCES "Communities"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_CommunityDetail_CommunityID" ON public."CommunityDetail" USING btree ("CommunityID");
CREATE INDEX "IX_CommunityDetail_ID" ON public."CommunityDetail" USING btree ("ID");


-- public."CommunityGroup" definition

-- Drop table

-- DROP TABLE public."CommunityGroup";

CREATE TABLE public."CommunityGroup" (
	"GroupID" int4 NOT NULL,
	"CommunityID" int4 NOT NULL,
	CONSTRAINT "PK_CommunityGroup" PRIMARY KEY ("CommunityID", "GroupID"),
	CONSTRAINT "FK_CommunityGroup_Communities_CommunityID" FOREIGN KEY ("CommunityID") REFERENCES "Communities"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_CommunityGroup_Groups_GroupID" FOREIGN KEY ("GroupID") REFERENCES "Groups"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_CommunityGroup_GroupID" ON public."CommunityGroup" USING btree ("GroupID");


-- public."ExtResourceFilters" definition

-- Drop table

-- DROP TABLE public."ExtResourceFilters";

CREATE TABLE public."ExtResourceFilters" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"ModuleID" int4 NOT NULL,
	"ValueType" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"Key" varchar(100) NULL,
	"Labels" varchar(2048) NULL,
	"ValidValues" varchar(2048) NULL,
	"ValidValuesLabels" varchar(2097152) NULL,
	"IsRequired" bool NOT NULL,
	CONSTRAINT "PK_ExtResourceFilters" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_ExtResourceFilters_Modules_ModuleID" FOREIGN KEY ("ModuleID") REFERENCES "Modules"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_ExtResourceFilters_ID" ON public."ExtResourceFilters" USING btree ("ID");
CREATE INDEX "IX_ExtResourceFilters_ModuleID" ON public."ExtResourceFilters" USING btree ("ModuleID");


-- public."ExtResourceGroups" definition

-- Drop table

-- DROP TABLE public."ExtResourceGroups";

CREATE TABLE public."ExtResourceGroups" (
	"ID" serial NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"OriginatorModule" uuid NOT NULL,
	"Guid" uuid NOT NULL,
	"ModuleID" int4 NOT NULL,
	"Name" varchar(100) NULL,
	"Description" varchar(1024) NULL,
	"Communities" _uuid NULL,
	CONSTRAINT "PK_ExtResourceGroups" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_ExtResourceGroups_Modules_ModuleID" FOREIGN KEY ("ModuleID") REFERENCES "Modules"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_ExtResourceGroups_ID" ON public."ExtResourceGroups" USING btree ("ID");
CREATE INDEX "IX_ExtResourceGroups_ModuleID" ON public."ExtResourceGroups" USING btree ("ModuleID");


-- public."JoinedCommunity" definition

-- Drop table

-- DROP TABLE public."JoinedCommunity";

CREATE TABLE public."JoinedCommunity" (
	"UserID" int4 NOT NULL,
	"CommunityID" int4 NOT NULL,
	"JoinTimestamp" timestamp NOT NULL,
	"IsCommunityAdmin" bool NOT NULL,
	CONSTRAINT "PK_JoinedCommunity" PRIMARY KEY ("CommunityID", "UserID"),
	CONSTRAINT "FK_JoinedCommunity_Communities_CommunityID" FOREIGN KEY ("CommunityID") REFERENCES "Communities"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_JoinedCommunity_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_JoinedCommunity_UserID" ON public."JoinedCommunity" USING btree ("UserID");


-- public."LicenseDocuments" definition

-- Drop table

-- DROP TABLE public."LicenseDocuments";

CREATE TABLE public."LicenseDocuments" (
	"ID" serial NOT NULL,
	"UserID" int4 NOT NULL,
	"IssuingAuthorityID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"LicenseNumber" varchar(25) NOT NULL,
	"IssueDate" timestamp NOT NULL,
	"ExpiryDate" timestamp NULL,
	"IsValid" bool NULL,
	"Category" varchar(15) NULL,
	"EquivalentCategory" varchar(15) NULL,
	"Type" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"Notes" varchar(500) NULL,
	"Attachments" _uuid NULL,
	CONSTRAINT "PK_LicenseDocuments" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_LicenseDocuments_IssuingAuthorities_IssuingAuthorityID" FOREIGN KEY ("IssuingAuthorityID") REFERENCES "IssuingAuthorities"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_LicenseDocuments_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_LicenseDocuments_ID" ON public."LicenseDocuments" USING btree ("ID");
CREATE INDEX "IX_LicenseDocuments_IssuingAuthorityID" ON public."LicenseDocuments" USING btree ("IssuingAuthorityID");
CREATE INDEX "IX_LicenseDocuments_UserID" ON public."LicenseDocuments" USING btree ("UserID");


-- public."MessageLog" definition

-- Drop table

-- DROP TABLE public."MessageLog";

CREATE TABLE public."MessageLog" (
	"ID" serial NOT NULL,
	"UserID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"MessageFormat" int4 NOT NULL,
	"NotificationType" int4 NOT NULL,
	"LastSendingAttempt" timestamp NULL,
	"SendingAttemptResult" bool NULL,
	CONSTRAINT "PK_MessageLog" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_MessageLog_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_MessageLog_ID" ON public."MessageLog" USING btree ("ID");
CREATE INDEX "IX_MessageLog_UserID" ON public."MessageLog" USING btree ("UserID");


-- public."NotificationPreferences" definition

-- Drop table

-- DROP TABLE public."NotificationPreferences";

CREATE TABLE public."NotificationPreferences" (
	"ID" serial NOT NULL,
	"UserID" int4 NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"NotificationType" int4 NOT NULL,
	"IsEMail" bool NOT NULL,
	"IsSMS" bool NOT NULL,
	"IsPush" bool NOT NULL,
	"LastUpdatedBy" varchar(100) NULL,
	CONSTRAINT "PK_NotificationPreferences" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_NotificationPreferences_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_NotificationPreferences_ID" ON public."NotificationPreferences" USING btree ("ID");
CREATE INDEX "IX_NotificationPreferences_UserID" ON public."NotificationPreferences" USING btree ("UserID");


-- public."Place" definition

-- Drop table

-- DROP TABLE public."Place";

CREATE TABLE public."Place" (
	"ID" serial NOT NULL,
	"UserID" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"Name" varchar(250) NULL,
	"Labels" varchar(2048) NULL,
	"AdditionalData" varchar(2048) NULL,
	"Address" varchar(1024) NULL,
	"Latitude" float8 NULL,
	"Longitude" float8 NULL,
	"IsFreefloatingArea" bool NOT NULL DEFAULT false,
	"IsRoundtripArea" bool NOT NULL DEFAULT false,
	CONSTRAINT "PK_Place" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Place_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_Place_ID" ON public."Place" USING btree ("ID");
CREATE INDEX "IX_Place_UserID" ON public."Place" USING btree ("UserID");


-- public."Roles" definition

-- Drop table

-- DROP TABLE public."Roles";

CREATE TABLE public."Roles" (
	"ID" serial NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"ParentID" int4 NULL,
	"Name" varchar(250) NULL,
	"Slug" varchar(50) NULL,
	"IsBackend" bool NOT NULL,
	"IsFrontend" bool NOT NULL,
	"IsOperator" bool NOT NULL,
	CONSTRAINT "PK_Roles" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Roles_Modules_ParentID" FOREIGN KEY ("ParentID") REFERENCES "Modules"("ID") ON DELETE RESTRICT
);
CREATE INDEX "IX_Roles_ID" ON public."Roles" USING btree ("ID");
CREATE INDEX "IX_Roles_ParentID" ON public."Roles" USING btree ("ParentID");
CREATE UNIQUE INDEX "IX_Roles_Slug" ON public."Roles" USING btree ("Slug");


-- public."ServiceRules" definition

-- Drop table

-- DROP TABLE public."ServiceRules";

CREATE TABLE public."ServiceRules" (
	"ID" serial NOT NULL,
	"GroupID" int4 NOT NULL,
	"ExtResourceGroupID" int4 NOT NULL,
	"EquivalentCategoriesAllowed" varchar(200) NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	CONSTRAINT "PK_ServiceRules" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_ServiceRules_ExtResourceGroups_ExtResourceGroupID" FOREIGN KEY ("ExtResourceGroupID") REFERENCES "ExtResourceGroups"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_ServiceRules_Groups_GroupID" FOREIGN KEY ("GroupID") REFERENCES "Groups"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_ServiceRules_ExtResourceGroupID" ON public."ServiceRules" USING btree ("ExtResourceGroupID");
CREATE INDEX "IX_ServiceRules_GroupID" ON public."ServiceRules" USING btree ("GroupID");
CREATE INDEX "IX_ServiceRules_ID" ON public."ServiceRules" USING btree ("ID");


-- public."TransportationRequests" definition

-- Drop table

-- DROP TABLE public."TransportationRequests";

CREATE TABLE public."TransportationRequests" (
	"ID" serial NOT NULL,
	"UserID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"StartLocationQuery" varchar(250) NULL,
	"EndLocationQuery" varchar(250) NULL,
	"StartTime" timestamp NOT NULL,
	"EndTime" timestamp NULL,
	"Guid" uuid NOT NULL,
	"ExternalResourceGroups" _uuid NULL,
	"LastRefreshed" timestamp NULL,
	CONSTRAINT "PK_TransportationRequests" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_TransportationRequests_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE UNIQUE INDEX "IX_TransportationRequests_Guid" ON public."TransportationRequests" USING btree ("Guid");
CREATE INDEX "IX_TransportationRequests_ID" ON public."TransportationRequests" USING btree ("ID");
CREATE INDEX "IX_TransportationRequests_UserID" ON public."TransportationRequests" USING btree ("UserID");


-- public."UserGroup" definition

-- Drop table

-- DROP TABLE public."UserGroup";

CREATE TABLE public."UserGroup" (
	"GroupID" int4 NOT NULL,
	"UserID" int4 NOT NULL,
	CONSTRAINT "PK_UserGroup" PRIMARY KEY ("GroupID", "UserID"),
	CONSTRAINT "FK_UserGroup_Groups_GroupID" FOREIGN KEY ("GroupID") REFERENCES "Groups"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_UserGroup_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_UserGroup_UserID" ON public."UserGroup" USING btree ("UserID");


-- public."UserRole" definition

-- Drop table

-- DROP TABLE public."UserRole";

CREATE TABLE public."UserRole" (
	"RoleID" int4 NOT NULL,
	"UserID" int4 NOT NULL,
	CONSTRAINT "PK_UserRole" PRIMARY KEY ("RoleID", "UserID"),
	CONSTRAINT "FK_UserRole_Roles_RoleID" FOREIGN KEY ("RoleID") REFERENCES "Roles"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_UserRole_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_UserRole_UserID" ON public."UserRole" USING btree ("UserID");


-- public."BulletinBoardDocuments" definition

-- Drop table

-- DROP TABLE public."BulletinBoardDocuments";

CREATE TABLE public."BulletinBoardDocuments" (
	"ID" serial NOT NULL,
	"CommunityID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"LastUpdated" timestamp NOT NULL,
	"LastUpdatedBy" text NULL,
	"Title" varchar(500) NULL,
	"Body" varchar(64000) NULL,
	"Url" varchar(1024) NULL,
	"Ordinal" int4 NOT NULL,
	"Type" int4 NOT NULL,
	"AttachmentId" uuid NULL,
	"LCID" int4 NOT NULL,
	CONSTRAINT "PK_BulletinBoardDocuments" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_BulletinBoardDocuments_Communities_CommunityID" FOREIGN KEY ("CommunityID") REFERENCES "Communities"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_BulletinBoardDocuments_CommunityID" ON public."BulletinBoardDocuments" USING btree ("CommunityID");
CREATE INDEX "IX_BulletinBoardDocuments_ID" ON public."BulletinBoardDocuments" USING btree ("ID");


-- public."LicenseDocumentValidations" definition

-- Drop table

-- DROP TABLE public."LicenseDocumentValidations";

CREATE TABLE public."LicenseDocumentValidations" (
	"ID" serial NOT NULL,
	"LicenseDocumentID" int4 NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"LastUpdatedBy" varchar(100) NULL,
	"LastUpdatedByID" uuid NOT NULL,
	"Notes" varchar(500) NULL,
	"Request" varchar(2048) NULL,
	"Response" varchar(2048) NULL,
	"IsSuccessful" bool NOT NULL,
	"IsDocumentValid" bool NULL,
	"IsManualValidation" bool NOT NULL DEFAULT false,
	CONSTRAINT "PK_LicenseDocumentValidations" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_LicenseDocumentValidations_LicenseDocuments_LicenseDocument~" FOREIGN KEY ("LicenseDocumentID") REFERENCES "LicenseDocuments"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_LicenseDocumentValidations_ID" ON public."LicenseDocumentValidations" USING btree ("ID");
CREATE INDEX "IX_LicenseDocumentValidations_LicenseDocumentID" ON public."LicenseDocumentValidations" USING btree ("LicenseDocumentID");


-- public."MovementAuthorities" definition

-- Drop table

-- DROP TABLE public."MovementAuthorities";

CREATE TABLE public."MovementAuthorities" (
	"ID" serial NOT NULL,
	"TransportationRequestID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"StartTime" timestamp NOT NULL,
	"EndTime" timestamp NOT NULL,
	"EndLocationName" varchar(250) NULL,
	"EndLocationAddress" varchar(250) NULL,
	"StartLocationName" varchar(250) NULL,
	"StartLocationAddress" varchar(250) NULL,
	"StartLocationLatitude" float8 NULL,
	"StartLocationLongitude" float8 NULL,
	"EndLocationLatitude" float8 NULL,
	"EndLocationLongitude" float8 NULL,
	"OriginatorKey" varchar(100) NULL,
	"OriginatorModule" uuid NOT NULL,
	"LastUpdated" timestamp NOT NULL,
	"Guid" uuid NOT NULL,
	"IsConfirmed" bool NOT NULL,
	"IsReadyToStart" bool NOT NULL,
	"MaxTripCount" int4 NOT NULL,
	"MinValidStartTime" timestamp NOT NULL,
	"MaxValidEndTime" timestamp NOT NULL,
	"MaxValidStartTime" timestamp NOT NULL,
	"EquipmentReference" varchar(150) NULL,
	"ServiceReference" varchar(150) NULL,
	CONSTRAINT "PK_MovementAuthorities" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_MovementAuthorities_TransportationRequests_TransportationRe~" FOREIGN KEY ("TransportationRequestID") REFERENCES "TransportationRequests"("ID") ON DELETE CASCADE
);
CREATE UNIQUE INDEX "IX_MovementAuthorities_Guid" ON public."MovementAuthorities" USING btree ("Guid");
CREATE INDEX "IX_MovementAuthorities_ID" ON public."MovementAuthorities" USING btree ("ID");
CREATE INDEX "IX_MovementAuthorities_TransportationRequestID" ON public."MovementAuthorities" USING btree ("TransportationRequestID");


-- public."MovementDetails" definition

-- Drop table

-- DROP TABLE public."MovementDetails";

CREATE TABLE public."MovementDetails" (
	"ID" serial NOT NULL,
	"MovementAuthorityID" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"OriginatorKey" varchar(100) NULL,
	"Labels" varchar(2048) NULL,
	"ValueLabels" varchar(2048) NULL,
	"Value" varchar(1024) NULL,
	"ValueType" int4 NOT NULL,
	CONSTRAINT "PK_MovementDetails" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_MovementDetails_MovementAuthorities_MovementAuthorityID" FOREIGN KEY ("MovementAuthorityID") REFERENCES "MovementAuthorities"("ID") ON DELETE CASCADE
);
CREATE UNIQUE INDEX "IX_MovementDetails_Guid" ON public."MovementDetails" USING btree ("Guid");
CREATE INDEX "IX_MovementDetails_ID" ON public."MovementDetails" USING btree ("ID");
CREATE INDEX "IX_MovementDetails_MovementAuthorityID" ON public."MovementDetails" USING btree ("MovementAuthorityID");


-- public."TRFilters" definition

-- Drop table

-- DROP TABLE public."TRFilters";

CREATE TABLE public."TRFilters" (
	"ID" serial NOT NULL,
	"TransportationRequestID" int4 NOT NULL,
	"ExtResourceFilterID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"Value" varchar(250) NULL,
	CONSTRAINT "PK_TRFilters" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_TRFilters_ExtResourceFilters_ExtResourceFilterID" FOREIGN KEY ("ExtResourceFilterID") REFERENCES "ExtResourceFilters"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_TRFilters_TransportationRequests_TransportationRequestID" FOREIGN KEY ("TransportationRequestID") REFERENCES "TransportationRequests"("ID") ON DELETE CASCADE
);
CREATE INDEX "IX_TRFilters_ExtResourceFilterID" ON public."TRFilters" USING btree ("ExtResourceFilterID");
CREATE INDEX "IX_TRFilters_ID" ON public."TRFilters" USING btree ("ID");
CREATE INDEX "IX_TRFilters_TransportationRequestID" ON public."TRFilters" USING btree ("TransportationRequestID");


-- public."Trips" definition

-- Drop table

-- DROP TABLE public."Trips";

CREATE TABLE public."Trips" (
	"ID" serial NOT NULL,
	"MovementAuthorityID" int4 NOT NULL,
	"UserID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"CreatedDate" timestamp NOT NULL,
	"Guid" uuid NOT NULL,
	"IsCompleted" bool NOT NULL,
	"IsBillable" bool NOT NULL,
	"BillableReason" varchar(250) NULL,
	"StartLocationName" varchar(250) NULL,
	"EndLocationName" varchar(250) NULL,
	"StartLocationLatitude" float8 NULL,
	"StartLocationLongitude" float8 NULL,
	"EndLocationLatitude" float8 NULL,
	"EndLocationLongitude" float8 NULL,
	"EndTimestamp" timestamp NULL,
	"StartTimestamp" timestamp NOT NULL,
	"Status" int4 NOT NULL,
	"Index" int4 NOT NULL,
	"EquipmentReference" varchar(150) NULL,
	"ServiceReference" varchar(150) NULL,
	"ActualDurationSeconds" int4 NOT NULL DEFAULT 0,
	"ActualMileageKm" int4 NOT NULL DEFAULT 0,
	CONSTRAINT "PK_Trips" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_Trips_MovementAuthorities_MovementAuthorityID" FOREIGN KEY ("MovementAuthorityID") REFERENCES "MovementAuthorities"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_Trips_Users_UserID" FOREIGN KEY ("UserID") REFERENCES "Users"("ID") ON DELETE CASCADE
);
CREATE UNIQUE INDEX "IX_Trips_Guid" ON public."Trips" USING btree ("Guid");
CREATE INDEX "IX_Trips_ID" ON public."Trips" USING btree ("ID");
CREATE INDEX "IX_Trips_MovementAuthorityID" ON public."Trips" USING btree ("MovementAuthorityID");
CREATE INDEX "IX_Trips_UserID" ON public."Trips" USING btree ("UserID");


-- public."TripDetails" definition

-- Drop table

-- DROP TABLE public."TripDetails";

CREATE TABLE public."TripDetails" (
	"ID" serial NOT NULL,
	"TripID" int4 NOT NULL,
	"DeletionDate" timestamp NULL,
	"Guid" uuid NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"OriginatorKey" varchar(100) NULL,
	"Labels" varchar(2048) NULL,
	"ValueLabels" varchar(2048) NULL,
	"Value" varchar(1024) NULL,
	"ValueType" int4 NOT NULL,
	CONSTRAINT "PK_TripDetails" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_TripDetails_Trips_TripID" FOREIGN KEY ("TripID") REFERENCES "Trips"("ID") ON DELETE CASCADE
);
CREATE UNIQUE INDEX "IX_TripDetails_Guid" ON public."TripDetails" USING btree ("Guid");
CREATE INDEX "IX_TripDetails_ID" ON public."TripDetails" USING btree ("ID");
CREATE INDEX "IX_TripDetails_TripID" ON public."TripDetails" USING btree ("TripID");


-- public."TripEvents" definition

-- Drop table

-- DROP TABLE public."TripEvents";

CREATE TABLE public."TripEvents" (
	"ID" serial NOT NULL,
	"TripID" int4 NOT NULL,
	"Guid" uuid NOT NULL,
	"CreatedDate" timestamp NOT NULL,
	"Timestamp" timestamp NOT NULL,
	"Latitude" float8 NULL,
	"Longitude" float8 NULL,
	"Source" int4 NOT NULL,
	"Type" int4 NOT NULL,
	"AdditionalData" varchar(1024) NULL,
	"OriginatorKey" uuid NULL,
	CONSTRAINT "PK_TripEvents" PRIMARY KEY ("ID"),
	CONSTRAINT "FK_TripEvents_Trips_TripID" FOREIGN KEY ("TripID") REFERENCES "Trips"("ID") ON DELETE CASCADE
);
CREATE UNIQUE INDEX "IX_TripEvents_Guid" ON public."TripEvents" USING btree ("Guid");
CREATE INDEX "IX_TripEvents_ID" ON public."TripEvents" USING btree ("ID");
CREATE INDEX "IX_TripEvents_TripID" ON public."TripEvents" USING btree ("TripID");

INSERT INTO "Roles" ("ID", "LastUpdated", "ParentID", "Name", "Slug", "IsBackend", "IsFrontend", "IsOperator") VALUES
(1,	'2020-01-23 08:31:27.568835',	null,	'Administrator',	'Admin',	'1',	'0',	'1'),
(2,	'2020-01-23 08:31:27.568837',	null,	'Power User',	'PowerUser',	'1',	'0',	'1'),
(3,	'2020-01-23 08:31:27.568838',	null,	'End User',	'EndUser',	'0',	'1',	'0'),
(4,	'2020-07-30 13:08:03.337208',	null,	'Vehicles Administrator',	'V-Admin',	'1',	'0',	'1'),
(5,	'2020-07-30 13:08:03.337209',	null,	'Vehicles Power User',	'V-PowerUser',	'1',	'0',	'1'),
(6,	'2020-07-30 13:08:03.337206',	null,	'Control Room User',	'ControlRoom',	'1',	'0',	'1');