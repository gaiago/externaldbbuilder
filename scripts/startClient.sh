#!/usr/bin/env bash

dbName=`docker ps | grep "5432/tcp" | awk '{print $11}'`
docker run -d --rm --link "$dbName:db" --name gaiagoDbClient -p 8090:8080 adminer